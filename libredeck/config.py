#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
LibreDeck configuration
"""

# Imports ===============================================================#

import os
import platform
import webbrowser

import lxml.etree as ET

from libredeck.common import fromxml_yesno,toxml_yesno

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Classes ===============================================================#

class Settings:

    def __init__(self, filepath=None):
        self.ui = {
            "dark_theme": False,
            "hide_button_title": False
        }

        self.ux = {
            "deck_startup": False,
            "browser": False,
            "version_startup": True
        }

        self.run = {
            "system": platform.system()
        }


        self.recent = []

        self.filepath = filepath

    def update_recent(self):
        """
        Clean the recently opened files list of files that do not exists
        anymore.
        """

        if self.recent:

            for r in self.recent:
                if not os.path.exists(os.path.realpath(r)):
                    self.recent.pop(r)

            # The recently opened files list must not exceed 5 files.
            # Yes, this is absolutely arbitrary. We could make this an option
            # but LibreDeck is not a KDE-like software.

            if len(self.recent) > 5:
                self.recent = self.recent[:5]

    def add_recent(self, recent_file):
        """
        Add a recently opened file to the ... recently opened files list.

        :type recent_file: string
        :param recent_file: Filepath of the recent file
        :rtype: boolean
        :returns: False if the recent file doesn't exists.
        """

        self.update_recent()

        if os.path.exists(recent_file):

            # If recent_file is already in the list, we remove it at its
            # previous index, so we can add it at index 0.
            if recent_file in self.recent:
                idx = self.recent.index(recent_file)
                self.recent.pop(idx)

            # The recently opened files list must not exceed 5 files.
            # Yes, this is absolutely arbitrary. We could make this an option
            # but LibreDeck is not a KDE-like software.
            if len(self.recent) >= 5:
                self.recent.pop(self.recent[-1])

            # Adds recent_file at index 0 of the recently opened files list.
            self.recent = [recent_file] + self.recent

            return True
        else:
            return False

    def set_browser(self):
        webbrowser.register(
            self.ux["browser"], None,
            webbrowser.get(self.ux["browser"]),
            preferred=True
        )

    def fromxml(self, xml):

        def ux(element, config):

            for sub in element:

                if sub.tag == "deck_startup":
                    config.ux["deck_startup"] = fromxml_yesno(sub.text)

                if sub.tag == "version_startup":
                    config.ux["version_startup"] = fromxml_yesno(sub.text)

                if sub.tag == "browser":
                    config.ux["browser"] = sub.text.strip()

            return config

        def ui(element, config):

            for sub in element:

                if sub.tag == "dark_theme":
                    config.ui["dark_theme"] = fromxml_yesno(sub.text)

                if sub.tag == "hide_button_title":
                    config.ui["hide_button_title"] = fromxml_yesno(sub.text)

            return config

        def recent(element, config):

            for sub in main:

                if sub.tag == "file":
                    config.add_recent(os.path.realpath(sub.text.strip()))

            return config

        if xml.tag == "settings":

            for main in xml:
                if main.tag == "recentfiles":
                    self = recent(main, self)
                if main.tag == "ux":
                    self = ux(main, self)
                if main.tag == "ui":
                    self = ui(main, self)

            if self.ux["browser"]:
                self.set_browser()

            return True

        return False

    def fromfile(self, filepath=None):
        """
        Reads settings from filepath, uses Settings.filepath if
        filepath is None.

        :rtype: boolean
        :returns: True if parsing was OK
        """

        if filepath is None:
            if self.filepath:
                filepath = self.filepath

        if filepath is not None:
            xml = ET.parse(filepath).getroot()

            return self.fromxml(xml)

        return False

    def toxml(self):

        def cfg_bool(main, config, setting, domain):
            if domain == "ui":
                domain = config.ui
            if domain == "ux":
                domain = config.ux

            s = ET.Element(setting)
            s.text = toxml_yesno(domain[setting])
            main.append(s)

            return main

        def ux_bool(main, config, setting):
            return cfg_bool(main, config, setting, "ux")
        def ui_bool(main, config, setting):
            return cfg_bool(main, config, setting, "ui")

        settings = ET.Element("settings")

        #--- Recent files -------------------------------------------

        self.update_recent()

        if self.recent:
            recent = ET.Element("recentfiles")

            for r in self.recent:
                rx = ET.Element("file")
                rx.text = r
                recent.append(rx)

            settings.append(recent)

        #--- UI -----------------------------------------------------

        ui = ET.Element("ui")
        ui = ui_bool(ui, self, "dark_theme")
        ui = ui_bool(ui, self, "hide_button_title")

        #--- UX -----------------------------------------------------

        ux = ET.Element("ux")
        ux = ux_bool(ux, self, "deck_startup")
        ux = ux_bool(ux, self, "version_startup")

        if self.ux["browser"]:
            browser = ET.Element("browser")
            browser.text = self.ux["browser"]
            ux.append(browser)

        #--- UX -----------------------------------------------------

        settings.append(ui)
        settings.append(ux)

        return settings

    def toxmlstr(self):
        xml = '<?xml version="1.0" encoding="utf-8"?>'
        xml += "\n\n"

        xml += ET.tostring(
            self.toxml(),
            pretty_print=True,
            encoding="unicode"
        )

        return xml

    def tofile(self, filepath=None):

        if filepath is None:
            if self.filepath:
                filepath = self.filepath

        if filepath is not None:
            xml = self.toxml()

            with open(filepath, "w", encoding="utf8") as xf:
                xf.write(self.toxmlstr())

            return True

        return False

# vim:set shiftwidth=4 softtabstop=4:
