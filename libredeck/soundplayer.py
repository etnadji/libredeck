#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
Sound player class for LibreDeck
"""

# Variables globales 1 ==================================================#

# How LibreDeck play sounds. Adjusted from os.name.
# - GStreamer ("gst") for GNU/Linux
# - winsound ("winsound") for Windows
SOUND_PLAYER = "gst"

# Imports ===============================================================#

import os

#--- Import module according to os.name ------------------------

if os.name == "nt":
    # --- Windows : winsound ------------------------------------
    import winsound
    SOUND_PLAYER = "winsound"
else:
    #--- GNU/Linux : PyGObject GStreamer -----------------------
    import gi
    gi.require_version('Gst', '1.0')

    from gi.repository import Gst
    Gst.init(None)

# Variables globales 2 ==================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

COMPATIBILITY = {
    "Windows": True,
    "Linux": True,
    "Darwin": True
}

ADJUST_COMPATIBILITY = {
    "Windows": False,
    "Linux": True,
    "Darwin": True
}

# Classes ===============================================================#

class SoundPlayer:

    def __init__(self, sound_player="gst"):
        self.sound_player = sound_player
        self.player = False

    def play(self, uri, volume=1, app=False):
        """
        With GStreamer:
            Use GStreamer playerbin.

            We recreate the player at every use of the method to
            make sound spamming a feature.

        With winsound:
            Use winsound.PlaySound

            Only play one sound at once. Play nothing if sound file
            is not WAV.
        """

        if self.sound_player == "gst":
            self.player = Gst.ElementFactory.make("playbin", "player")
            self.player.set_property('uri', uri)

            if app:
                # If the board action has no volume defined (-1),
                # the global volume of LibreDeck is used.
                if volume < 0:
                    volume = app.win.volume.get_value()

            self.player.set_property("volume", volume)
            self.player.set_state(Gst.State.PLAYING)

        elif self.sound_player == "winsound":
            winsound.PlaySound(
                uri,
                winsound.SND_FILENAME | winsound.SND_NODEFAULT | winsound.SND_ASYNC
            )

        else:
            if app.verbose:
                print(
                    "Unknown sound player for LibreDeck : {}".format(
                        self.sound_player
                    )
                )

    def set_property(self, propname, propvalue):
        """
        Alias for GStreamer playbin set_property if Gstreamer used.
        """

        if self.sound_player == "gst":
            self.player.set_property(propname, propvalue)

    def set_state(self, state):
        """
        Alias for GStreamer playbin set_state if Gstreamer used.
        """

        self.player.set_state(state)

    def stop(self):
        """
        Stop the sound.
        """

        if self.sound_player == "gst":
            # GStreamer player stop
            if self.player:
                self.player.set_state(Gst.State.NULL)

        elif self.sound_player == "winsound":
            # Winsound player stop
            winsound.PlaySound(None, winsound.SND_PURGE)

        else:
            print(
                "Unknown sound player for LibreDeck : {}".format(
                    self.sound_player
                )
            )

# vim:set shiftwidth=4 softtabstop=4:
