#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
Mixins for actions handlers of LibreDeck
"""

# Imports ===============================================================#

import os
import operator
import functools

#--- GTK ----------------------------------------------

import gi

gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GdkPixbuf

#--- LXML ---------------------------------------------

import lxml.etree as ET

#--- LibreDeck ----------------------------------------

from libredeck.common import fromxml_yesno,toxml_yesno

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Classes ===============================================================#

class Mixin:
    """
    Abstract class for mixin.
    """

    def __init__(self):
        pass

    def mixin_obj(self, mixin_id, obj):
        return self.mixins[mixin_id].get_object(obj)

    def add_mixin_builder(self, filepath, mixin_id):
        """
        Adds a GTK builder to the mixins (builder) dictionnary.

        :type filepath: string
        :type mixin_id: string
        """

        self.mixins[mixin_id] = Gtk.Builder()
        self.mixins[mixin_id].add_from_file(self.application.ui_file(filepath))
        self.mixins[mixin_id].connect_signals(self)

    def get_mixin_builder(self, mixin_id):
        return self.mixins[mixin_id]


class MixinKeyboardShortcut(Mixin):

    def __init__(self):
        Mixin.__init__(self)

        self.add_mixin_builder("mixin_shortcut", "action-shortcut")
        self.keyboard_shorcut = None
        self.keyboard_modifiers = []

    def prepare_config(self):
        builder = self.get_mixin_builder("action-shortcut")

        mixin_tab = builder.get_object("mixin_box")

        if self.keyboard_shorcut is not None:
            self._redraw_keyboard_shortcut_label()

        self.add_to_config_notebook(mixin_tab, "Raccourci")

    def on_mst_setButton_clicked(self, button):
        builder = self.get_mixin_builder("action-shortcut")

        dialog = builder.get_object("shortcutSetterDialog")
        dialog.set_transient_for(self._cdialog["window"])
        dialog.show()

    def fromxml(self, xml):
        for element in xml:

            if element.tag == "shortcut":
                key = element.get("key")

                if key is not None:

                    keyval = None

                    if key != "none":
                        keyval = Gdk.keyval_from_name(key)

                    if keyval is not None:

                        modifiers = []

                        # NOTE Weirdly, making these lines works, but getting
                        # attribute in the next for loop fail to set keyboard

                        sup = element.get("super")
                        ctrl = element.get("ctrl")
                        shift = element.get("shift")
                        alt = element.get("alt")

                        for modifier,mask in [
                                [sup, Gdk.ModifierType.SUPER_MASK],
                                [shift, Gdk.ModifierType.SHIFT_MASK],
                                [ctrl, Gdk.ModifierType.CONTROL_MASK],
                                [alt, Gdk.ModifierType.MOD1_MASK]]:

                            if modifier is not None:
                                if modifier == "yes":
                                    modifiers.append(mask)

                        if modifiers:
                            self.keyboard_shorcut = (
                                keyval,
                                functools.reduce(operator.or_, modifiers)
                            )

                return True

        return False

    def toxml(self):
        xml = ET.Element("shortcut")

        if self.keyboard_shorcut is None:
            xml.attrib["key"] = "none"
        else:
            xml.attrib["key"] = Gdk.keyval_name(self.keyboard_shorcut[0])

        if self.keyboard_modifiers:

            for modifier in self.keyboard_modifiers:

                if modifier == "GDK_SUPER_MASK":
                    xml.attrib["super"] = "yes"
                if modifier == "GDK_CONTROL_MASK":
                    xml.attrib["ctrl"] = "yes"
                if modifier == "GDK_SHIFT_MASK":
                    xml.attrib["shift"] = "yes"
                if modifier == "GDK_MOD1_MASK":
                    xml.attrib["alt"] = "yes"

        return xml

    def on_mixin_box_key_press_event(self, box, event):

        hide_dialog,remove_shortcut = False,False

        keyval = event.get_keyval()[1]
        name = Gdk.keyval_name(keyval)

        if name in ["Escape", "BackSpace"]:
            hide_dialog = True

            if name == "BackSpace":
                remove_shortcut = True

        if name not in [
            'Shift_L', 'Shift_R', 'Control_L', 'Control_R', 'Meta_L',
            'Meta_R', 'Alt_L', 'Alt_R', 'Super_L', 'Super_R', 'Hyper_L',
            'Hyper_R', "BackSpace", "Escape"]:
            self.keyboard_shorcut = (
                keyval, event.state & (
                    Gdk.ModifierType.META_MASK | Gdk.ModifierType.SUPER_MASK | 
                    Gdk.ModifierType.HYPER_MASK | Gdk.ModifierType.SHIFT_MASK | 
                    Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.MOD1_MASK
                )
            )

            self._redraw_keyboard_shortcut_label()

            hide_dialog = True

        if hide_dialog:
            builder = self.get_mixin_builder("action-shortcut")
            dialog = builder.get_object("shortcutSetterDialog")
            dialog.hide()

        if remove_shortcut:
            self._reset_keyboard_shortcut_label()

        return True

    def shortcuted(self, accelgroup, appwindow, key, flags):
        pass

    def connect_shortcut(self):
        if self.keyboard_shorcut is not None:
            self.application.accelerators.connect(
                self.keyboard_shorcut[0],
                self.keyboard_shorcut[1], 0,
                self.shortcuted
            )

    def validate_config(self):
        self.connect_shortcut()

    def _reset_keyboard_shortcut_label(self):
        builder = self.get_mixin_builder("action-shortcut")
        label = builder.get_object("mst_label")
        label.set_markup("<i>non défini</i>")

        self.application.accelerators.disconnect_key(
            self.keyboard_shorcut[0], self.keyboard_shorcut[1],
        )

        self.keyboard_shorcut = None
        self.keyboard_modifiers = []

    def _redraw_keyboard_shortcut_label(self):

        # Get the human readable string of a modifier ------------------------

        def modifier_to_human(s):
            if s == 'GDK_SUPER_MASK':
                s = "Super"

            if s == 'GDK_MOD1_MASK':
                s = "Alt"

            if s == 'GDK_CONTROL_MASK':
                s = "Ctrl"

            if s == 'GDK_SHIFT_MASK':
                s = "Maj"

            if s == "ErgoHyper":
                s = "Ergodox Hyper"

            if s == "ErgoMeh":
                s = "Ergodox Meh"

            return s

        # Get the human readable string of the key ---------------------------

        human_key = Gdk.keyval_name(self.keyboard_shorcut[0])

        # Reading modifiers and making a human readable string of them -------

        # NOTE
        # Yeah, I would like to use an AccelLabel instead, but it does not 
        # work...

        modifiers = self.keyboard_shorcut[1]

        modifiers = [
            i for i in reversed(
                [k for k in str(modifiers).split()[1:][:-3] if k != "|"]
            )
        ]


        if modifiers == ['0']:
            modifiers = []

        self.keyboard_modifiers = modifiers

        # Transform Hyper key sequence into Ergodox Hyper key for readability
        if modifiers == [
            'GDK_SUPER_MASK', 'GDK_MOD1_MASK', 'GDK_CONTROL_MASK',
            'GDK_SHIFT_MASK']:
            modifiers = ["ErgoHyper"]

        # Transform Hyper key sequence into Ergodox Meh key for readability
        if modifiers == [
            "GDK_MOD1_MASK", "GDK_CONTROL_MASK", "GDK_SHIFT_MASK"]:
            modifiers = ["ErgoMeh"]

        modifiers = [modifier_to_human(m) for m in modifiers]

        shortcut_label = " + ".join(modifiers + [human_key])

        # Setting the keyboard shortcut label in the config tab --------------

        builder = self.get_mixin_builder("action-shortcut")
        label = builder.get_object("mst_label")
        label.set_markup(shortcut_label)


class MixinActionTitle(Mixin):

    def __init__(self, title_object=None):
        Mixin.__init__(self)

        self.add_mixin_builder("mixin_title", "action-title")
        self.title_object = title_object

        self.title = ""
        self.title_formating = {
            "bold": False, "italic": False, "color": False
        }

    def init_gui(self, hide=False):

        if self.title:
            self.set_title(
                self.title,
                self.title_formating["bold"], self.title_formating["italic"],
                self.title_formating["color"]
            )

        if hide:
            title_label = self._gladeobj(self.title_object)
            title_label.destroy()

    def color_to_hexa(self, color):
        def hexvalue(x):
            return max(0, min(x, 255))

        rgb = [
            int(c)
            for c in
            Gdk.RGBA.from_color(color).to_string()[4:][:-1].split(",")
        ]

        hexa = "#{0:02x}{1:02x}{2:02x}".format(
            hexvalue(rgb[0]), hexvalue(rgb[1]), hexvalue(rgb[2])
        )

        return hexa

    def formated_title(self):
        title = self.title

        if self.title_formating["italic"]:
            title = "<i>{}</i>".format(title)

        if self.title_formating["bold"]:
            title = "<b>{}</b>".format(title)

        if self.title_formating["color"]:
            title = "<span color='{}'>{}</span>".format(
                self.title_formating["color"], title
            )

        return title

    def set_title(self, title, bold=False, italic=False, color=False):
        self.title_formating = {
            "bold": bold, "italic": italic, "color": color
        }
        self.title = title

        title_label = self._gladeobj(self.title_object)
        title_label.set_markup(self.formated_title())

    def fromxml(self, xml):
        # Select the correct XML element (if xml = only <title> or
        # xml = the whole <action>)

        xml_element = False

        if xml.tag == "title":
            xml_element = xml

        if xml.tag == "action":
            for element in xml:
                if element.tag == "title":
                    xml_element = element
                    break

        # And if there is a correct XML element, parse it

        if isinstance(xml_element, ET._Element):

            self.title = xml_element.text.strip()

            for title_format in ["bold", "italic"]:
                att = xml_element.get(title_format)

                if att is not None:
                    self.title_formating[title_format] = fromxml_yesno(att)

            # I am the walrus, I AM THE WALRUS !
            if (color := xml_element.get("color")) is not None:
                self.title_formating["color"] = color

            return True

        # If not, well...

        return False

    def toxml(self):
        xml = ET.Element("title")

        for title_format in ["bold", "italic"]:
            if self.title_formating[title_format]:
                xml.attrib[title_format] = "yes"

        if self.title_formating["color"]:
            xml.attrib["color"] = self.title_formating["color"]

        xml.text = self.title.strip()

        return xml

    def on_mtc_titleColorButton_color_set(self, button):
        color = button.get_color()
        color = self.color_to_hexa(color)
        self.title_formating["color"] = color

    def validate_config(self):
        builder = self.get_mixin_builder("action-title")

        new_title = builder.get_object("mtc_titleEntry").get_text()
        bold_state = builder.get_object("mtc_titleBoldSwitch").get_state()
        ital_state = builder.get_object("mtc_titleItalicSwitch").get_state()

        dotitle = new_title == self.title
        dobold = self.title_formating["bold"] == bold_state
        doital = self.title_formating["italic"] == ital_state

        if dotitle or dobold or doital:
            self.set_title(
                new_title, bold_state, ital_state,
                self.title_formating["color"]
            )

    def prepare_config(self):
        builder = self.get_mixin_builder("action-title")

        title_entry = builder.get_object("mtc_titleEntry")
        title_entry.set_text(self.title)

        bold_switch = builder.get_object("mtc_titleBoldSwitch")
        bold_switch.set_state(self.title_formating["bold"])

        ital_switch = builder.get_object("mtc_titleItalicSwitch")
        ital_switch.set_state(self.title_formating["italic"])

        colorbutton = builder.get_object("mtc_titleColorButton")

        if self.title_formating["color"]:
            rgba = Gdk.RGBA()
            rgba.parse(self.title_formating["color"])
            colorbutton.set_rgba(rgba)

        mixin_tab = builder.get_object("mixin_box")

        self.add_to_config_notebook(mixin_tab, "Titre")


class MixinActionButton(Mixin):

    def __init__(self):
        Mixin.__init__(self)

        self.add_mixin_builder("mixin_button", "action-button")
        self.button_object = None

        self.button_image = None
        self.button_label = None
        self.button_mode = "text"

    def fromxml(self, xml):

        for element in xml:

            if element.tag == "button":

                if (mode := element.get("mode")) is not None:
                    self.set_mode(mode.strip(), False)

                for sub in element:

                    if sub.tag == "label":

                        if self.button_mode == "text":
                            self.button_label = sub.text.strip()

                    if sub.tag == "image":

                        if self.button_mode == "image":
                            self.button_image = sub.text.strip()

                return True

        return False

    def toxml(self):
        xml = ET.Element("button")

        xml.attrib["mode"] = self.button_mode

        if self.button_label is not None:

            if self.button_label:
                label = ET.Element("label")
                label.text = self.button_label.strip()
                xml.append(label)

        if self.button_image:
            image = ET.Element("image")
            image.text = self.button_image.strip()
            xml.append(image)

        return xml

    def init_gui(self):
        self.set_mode(self.button_mode)

        if self.button_mode == "text":
            self.set_button_label(self.button_label)

        if self.button_mode == "image":
            self.set_button_image(self.button_image)

    def _adjust_config_tab_buttonmode(self, mode):
        builder = self.get_mixin_builder("action-button")

        if mode == "image":
            seq = [False, True]
        if mode == "text":
            seq = [True, False]

        if seq:
            for w in ["button_text", "button_text_label"]:
                builder.get_object(w).set_sensitive(seq[0])

            for w in ["button_image", "button_image_label"]:
                builder.get_object(w).set_sensitive(seq[1])

    def on_button_mode_changed(self, combobox):
        mode = int(combobox.get_active_id())

        if mode == 0:
            self._adjust_config_tab_buttonmode("image")

        if mode == 1:
            self._adjust_config_tab_buttonmode("text")

    def prepare_config(self):
        builder = self.get_mixin_builder("action-button")

        #=== Mode

        button_mode = builder.get_object("button_mode")

        if self.button_mode == "image":
            button_mode.set_active_id("0")
        else:
            button_mode.set_active_id("1")

        button_label = builder.get_object("button_text")

        if self.button_label is not None:
            button_label.set_text(self.button_label)

        self._adjust_config_tab_buttonmode(self.button_mode)

        #=== Image

        button_image = builder.get_object("button_image")

        if self.button_image is not None:
            button_image.set_filename(self.button_image)

        #=== Config tab

        mixin_tab = builder.get_object("mixin_box")
        self.add_to_config_notebook(mixin_tab, "Bouton")

    def set_mode(self, mode, ui=True):

        if mode in ["text", "image"]:
            self.button_mode = mode

            if ui:

                builder = self.get_mixin_builder("action-button")
                button_obj = self._gladeobj(self.button_object)

                if mode == "text":
                    button_obj.set_relief(Gtk.ReliefStyle.NORMAL)
                    self.set_button_label(self.button_label)

                if mode == "image":
                    button_obj.set_relief(Gtk.ReliefStyle.NONE)
                    self.set_button_image(self.button_image)

    def validate_config(self):
        builder = self.get_mixin_builder("action-button")

        #=== Mode

        mode = int(builder.get_object("button_mode").get_active_id())
        newmode = ""

        if mode == 0 and self.button_mode == "text":
            newmode = "image"

        if mode == 1 and self.button_mode == "image":
            newmode = "text"

        if newmode:
            self.set_mode(newmode)

        #=== Image

        button_image = builder.get_object("button_image").get_filename()

        if self.button_image != button_image:
            if self.button_mode == "image":
                self.set_button_image(button_image)
            else:
                self.button_image = button_image

    def set_button_label(self, label="", keep=True):
        if label is not None:
            if keep:
                self.button_label = label

            button_obj = self._gladeobj(self.button_object)
            button_obj.set_label(label)

    def set_button_image(self, filename, keep=True):
        if keep:
            self.button_image = filename

        button_obj = self._gladeobj(self.button_object)
        setimage = True

        try:
            image = Gtk.Image.new_from_pixbuf(
                GdkPixbuf.Pixbuf.new_from_file_at_scale(
                    filename, 120, -1, True
                )
            )

        except gi.repository.GLib.GError:
            print(filename, "doesn't exists.")

            return False

        except TypeError:
            setimage = False

        if setimage:
            button_obj.set_image(image)
            self.set_button_label(keep=False)

            self.button_image = filename

        return True


class MixinSoundPlayer(Mixin):

    def __init__(self, ui="mixin_sound"):
        Mixin.__init__(self)

        self.add_mixin_builder(ui, "soundplayer")

        self.sound_uri = False
        self.sound_source = False

        self.sound_volume = float(1)
        self.sound_custom_volume = False

    def fromxml(self, xml):
        """
        :type xml: lxml.etree._Element
        """

        # Select the correct XML element (if xml = only <sound> or
        # xml = the whole <action>)

        xml_element = False

        if xml.tag == "sound":
            xml_element = xml

        if xml.tag == "action":
            for element in xml:
                if element.tag == "sound":
                    xml_element = element
                    break

        if isinstance(xml_element, ET._Element):

            if (source := xml_element.get("source")) is not None:
                source = source.strip()

                if source in ["distant", "local"]:
                    self.sound_source = source

            if (sound_volume := xml_element.get("volume")) is not None:
                self.sound_volume = float(sound_volume.strip())

            source_uri = xml_element.text.strip()

            if source_uri:
                self.sound_uri = source_uri

    def toxml(self):
        """
        :rtype xml: lxml.etree._Element
        """

        xml = ET.Element("sound")

        xml.attrib["source"] = self.sound_source

        if self.sound_volume != float(1):
            xml.attrib["volume"] = str(self.sound_volume)

        xml.text = self.sound_uri

        return xml

    def on_volume_switch_state_set(self, switch, state):
        """
        :type switch: Gtk.Switch
        :type state: boolean
        """

        self.sound_custom_volume = state
        builder = self.get_mixin_builder("soundplayer")
        builder.get_object("volume_scale").set_sensitive(state)

    def on_volume_scale_format_value(self, scale, value):
        """
        Set a custom display for the custom volume scale.

        :type scale: Gtk.Scale
        :type value: float
        """
        return "{} %".format(int(value * 100))

    def _adjust_config_tab_soundsource(self, source):
        """
        Adjust the config tab UI according to the sound source.

        :type source: string
        :param source: Sound source type, "local" or "distant"
        """

        builder = self.get_mixin_builder("soundplayer")

        if source == "local":
            seq = [False, True]
        if source == "distant":
            seq = [True, False]

        if seq:
            for w in ["sound_url", "sound_url_label"]:
                builder.get_object(w).set_sensitive(seq[0])
            for w in ["sound_file", "sound_file_label"]:
                builder.get_object(w).set_sensitive(seq[1])

    def on_sound_mode_changed(self, combobox):
        """
        Adjust the UI according to the changes to the sound source combobox.

        :type combobox: Gtk.ComboBoxText
        """

        mode = int(combobox.get_active_id())

        if mode == 0:
            self._adjust_config_tab_soundsource("local")

        if mode == 1:
            self._adjust_config_tab_soundsource("distant")

    def on_volume_scale_change_value(self, scale, scroll, value):
        """
        :type scale: Gtk.Scale
        :type scroll: Gtk.ScrollType
        :type value: float
        """
        if value > float(1):
            value = float(1)

        self.sound_volume = value

    def prepare_config(self, prepare_adjust=True, prepare_source=True):
        """
        Prepare the config tab for sound player and add it to the action
        configuration dialog.
        """

        builder = self.get_mixin_builder("soundplayer")

        if prepare_source:

            #=== Mode

            sound_mode = builder.get_object("sound_mode")

            if self.sound_source == "local" or self.sound_source is None:
                sound_mode.set_active_id("0")
            else:
                sound_mode.set_active_id("1")

            self._adjust_config_tab_soundsource(self.sound_source)

            #=== File

            sound_file = builder.get_object("sound_file")

            # Set the file chooser button value if the source is local

            if self.sound_source == "local":
                if self.sound_uri is not None and self.sound_uri:
                    sound_file.set_filename(self.sound_uri)

            # Set the filter according to OS limitations

            if os.name == "nt":
                sfilter = builder.get_object("audiofilterWindows")
            else:
                sfilter = builder.get_object("audiofilterAll")

            sound_file.set_filter(sfilter)

            #=== URL

            sound_url = builder.get_object("sound_url")

            if self.sound_source == "distant":
                if self.sound_uri is not None:
                    sound_url.set_text(self.sound_uri)

        #=== Volume

        if prepare_adjust:

            if os.name == "nt":
                # NOTE Custom volume is not available for Windows

                for w in [
                    "volume_switch", "volume_scale", "volume_title",
                    "volume_separator"]:
                    w = builder.get_object(w)
                    w.set_visible(False)
            else:
                switch = builder.get_object("volume_switch")
                switch.set_state(self.sound_custom_volume)

                scale = builder.get_object("volume_scale")
                scale.set_value(self.sound_volume)
                scale.set_sensitive(self.sound_custom_volume)

        #=== Config tab

        mixin_tab = builder.get_object("mixin_box")
        self.add_to_config_notebook(mixin_tab, "Son")

    def validate_config(self, validate_source=True):
        """
        Read configuration dialog tab and change attributes accordingly.
        """

        builder = self.get_mixin_builder("soundplayer")

        if validate_source:

            #=== Mode

            mode = int(builder.get_object("sound_mode").get_active_id())
            newmode = ""

            if self.sound_source:
                if mode == 0 and self.sound_source == "distant":
                    newmode = "local"
                if mode == 1 and self.sound_source == "local":
                    newmode = "distant"

                if newmode:
                    self.sound_source = newmode
            else:
                if mode == "0":
                    self.sound_source = "local"
                else:
                    self.sound_source = "distant"

            #=== File

            sound_file = builder.get_object("sound_file").get_filename()

            if self.sound_source == "local":
                if sound_file != self.sound_uri:
                    self.sound_uri = sound_file

            #=== URL

            sound_url = builder.get_object("sound_url").get_text()

            if self.sound_source == "distant":
                if sound_url != self.sound_uri:
                    self.sound_uri = sound_url

    def set_configured(self):
        self.action_configured = False

        if self.application.soundplayer:
            if self.sound_source and self.sound_uri:
                self.action_configured = True

    def stop(self):
        """Stops the sound."""

        if self.application.soundplayer:
            self.application.soundplayer.stop()

    def play(self):
        """Plays the sound."""

        if self.application.soundplayer:
            if self.sound_source and self.sound_uri:
                uri = self.sound_uri

                # Adds file:// protocol only for GStreamer
                # Windows doesn't understand that
                if self.application.soundplayer.sound_player == "gst":
                    if self.sound_source == "local":
                        uri = "file://" + uri

                self.application.soundplayer.play(uri, self.sound_volume)
            else:
                self.application.non_configured_action(self)

# vim:set shiftwidth=4 softtabstop=4:
