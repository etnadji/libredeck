#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
Interface top-levels, like decks (files) and LibreDeck welcome page.
"""

# Imports ===============================================================#

import os

#--- LXML ---------------------------------------------

import lxml.etree as ET

#--- LibreDeck ----------------------------------------

import libredeck.handlers as ldhandlers
import libredeck.actions as ldactions

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Classes ===============================================================#

class DeckHandler(ldhandlers.BaseHandler):

    def __init__(self, application, deck_id=0, filepath=None):
        super().__init__(False, application)

        # self.builder_file = "ui/deck.glade"
        self.builder_file = "deck"

        self.title = "Nouveau tableau"
        self.filepath = filepath

        self.deck_id = str(deck_id)

        self.actions = []

    def fromfile(self, filepath):
        xml = ET.parse(filepath).getroot()

        if len(xml):
            return self.fromxml(xml)

        return False

    def fromxml(self, xml):
        if xml.tag == "deck":

            # if (deck_title := xml.get("title")) is not None:
            deck_title = xml.get("title")
            if deck_title is not None:
                self.title = deck_title.strip()

            for main in xml:

                if main.tag == "actions":

                    for action in main:

                        if action.tag == "action":

                            # if (atype := action.get("type")) is not None:
                            atype = action.get("type")
                            if atype is not None:

                                aclass = ldactions.action_type_to_class(atype)

                                if aclass is not None:
                                    instance = aclass(self.application, self)
                                    instance.fromxml(action)
                                    instance.init_gui()
                                    instance.widget.guid = self.new_action_id()
                                    instance.connect_shortcut()
                                    self.actions.append(instance)

            return True

        return False

    def toxml(self):
        xml = ET.Element("deck")

        if self.title:
            xml.attrib["title"] = self.title

        actions = ET.Element("actions")

        for action in self.actions:
            ax = action.toxml()
            actions.append(ax)

        xml.append(actions)

        return xml

    def toxmlstr(self):
        xml = '<?xml version="1.0" encoding="utf-8"?>'
        xml += "\n\n"

        xml += ET.tostring(
            self.toxml(),
            pretty_print=True,
            encoding="unicode"
        )

        return xml

    def init_gui(self):
        ldhandlers.BaseHandler.init_gui(self)

        self.widget = self._gladeobj("deckBox")
        self.box = self._gladeobj("deckWidget")

        if self.filepath is not None:
            self.fromfile(self.filepath)

        self.widget.show_all()

        # stack = self.application._gladeobj("filesStackView")
        # stack.add_titled(self.widget, self.deck_id, self.title)

    def get_selected_action(self):
        selected_action = False

        children = self.box.get_selected_children()

        if children:

            flowchild = children[0]
            action_guid = flowchild.get_children()[0].guid

            for action in self.actions:
                if action.widget.guid == action_guid:
                    selected_action = action
                    break

            return selected_action,action_guid,flowchild

        else:
            return selected_action,None,None

    def on_deckWidget_selected_children_changed(self, param):
        selected_action,action_guid,flc = self.get_selected_action()
        self.application.update_on_selected_action(True)

    def add_actions(self):
        # for i in [1, 2]:
        #     action_handler = ldactions.AbstractButtonAction(self.application, self)
        #     action_handler.init_gui()
            # action_handler.widget.guid = self.new_action_id()
        #     self.actions.append(action_handler)

        # for i in [3, 4]:
        #     action_handler = ldactions.AbstractSwitchAction(self.application, self)
        #     action_handler.init_gui()
            # action_handler.widget.guid = self.new_action_id()
        #     self.actions.append(action_handler)

        birduri = "https://upload.wikimedia.org/wikipedia/commons/9/9c/"
        birduri += "Parus_major_15mars2011.ogg"

        birdsound = ldactions.SoundButtonAction(self.application, self)
        birdsound.init_gui()
        birdsound.widget.guid = self.new_action_id()
        birdsound.sound_uri = birduri
        birdsound.sound_source = "distant"
        birdsound.set_title("Mésange", True)
        self.actions.append(birdsound)

        # for i in [7]:
            # action_handler = ldactions.SoundButtonAction(self.application, self)
            # action_handler.init_gui()
            # action_handler.widget.guid = self.new_action_id()
            # action_handler.sound_player = self.application.soundplayer
            # self.actions.append(action_handler)

        for i in [9, 10]:
            action_handler = ldactions.ScriptAction(self.application, self)
            action_handler.init_gui()
            action_handler.widget.guid = self.new_action_id()
            self.actions.append(action_handler)

    def new_action_id(self):
        if self.actions:
            ids = [int(action.widget.guid) for action in self.actions]
            new_id = max(ids) + 1
        else:
            new_id = 1

        return new_id


class WelcomeHandler(ldhandlers.BaseHandler):

    def __init__(self, application):
        super().__init__(False, application)

        # self.builder_file = "ui/welcome.glade"
        self.builder_file = "welcome"

        self.deck_id = None

    def init_gui(self):
        ldhandlers.BaseHandler.init_gui(self)

        self.widget = self._gladeobj("welcomeWidget")

        last_deck = self._gladeobj("lastDeckButton")

        if self.application.settings.recent:
            filepath = self.application.settings.recent[0]
            deck_label = os.path.basename(filepath)
            last_deck.set_sensitive(True)
            last_deck.set_label(deck_label)

        self.widget.show_all()

        stack = self.application._gladeobj("filesStackView")
        stack.add_titled(self.widget, "libredeck-welcome", "Bienvenue")

    def on_lastDeckButton_clicked(self, button):
        if self.application.settings.recent:
            self.application.add_deck(
                self.application.settings.recent[0],
                isrecent=True
            )

    def on_aboutButton_clicked(self, button):
        self.application.about()

    def on_newDeckButton_clicked(self, button):
        self.application.new_deck()

    def on_openDeckButton_clicked(self, button):
        self.application.open_deck()

# vim:set shiftwidth=4 softtabstop=4:
