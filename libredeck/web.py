#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
Web action for LibreDeck.
"""

# Imports ===============================================================#

import os
import webbrowser

#--- GTK ----------------------------------------------

import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk

#--- LXML ---------------------------------------------

import lxml.etree as ET

#--- LibreDeck ----------------------------------------

import libredeck.abstactions as ldabst

from libredeck.common import fromxml_yesno,toxml_yesno

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Compatibility matching (against platform.system)
COMPATIBILITY = {
    "Windows": True,
    "Linux": True,
    "Darwin": True
}

# Classes ===============================================================#

class WebAction(ldabst.AbstractButtonAction):
    """
    Action which open a web URL

    :type application: ldeck.LibreDeck
    :param application: LibreDeck application
    :type deck: ldeck.DeckHandler
    :param deck: Current deck/file
    """

    def __init__(self, application, deck):
        ldabst.AbstractButtonAction.__init__(self, application, deck)
        self.datas["weburl"] = None

        self.action_type = "web"

        # self.config["file"] = "ui/web.glade"
        self.config["file"] = "web"

    def prepare_config(self):
        ldabst.AbstractButtonAction.prepare_config(self)

        if self.datas["weburl"]:
            main = self._gladeconfobj("url_entry")
            main.set_text(self.datas["weburl"])

        config_widget = self._gladeconfobj("config_box")
        self.add_to_config_notebook(config_widget, "Web")

    def fromxml(self, xml):
        ldabst.AbstractButtonAction.fromxml(self, xml)

        for element in xml:

            if element.tag == "web":
                self.datas["weburl"] = element.text.strip()
                self.action_configured = True

    def toxml(self):
        xml = ldabst.AbstractButtonAction.toxml(self)

        if self.datas["weburl"]:
            args = ET.Element("web")
            args.text = self.datas["weburl"]
            xml.append(args)

        return xml

    def validate_config(self):
        ldabst.AbstractButtonAction.validate_config(self)

        url = self._gladeconfobj("url_entry").get_text()

        if url != self.datas["weburl"]:
            self.datas["weburl"] = url
            self.action_configured = True

    def default(self):
        self.set_title("Webpage")
        self.set_button_label("Open")
        self.set_mode("text")
        self.datas["weburl"] = "https://etnadji.fr"
        self.action_configured = True

    def init_gui(self):
        ldabst.AbstractButtonAction.init_gui(self)

        if self.datas["weburl"]:
            self.action_configured = True

    def click(self):
        if self.action_configured:
            webbrowser.open_new_tab(self.datas["weburl"])
        else:
            self.application.non_configured_action(self)

# vim:set shiftwidth=4 softtabstop=4:
