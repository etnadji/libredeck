#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
GTK handlers and mixins for LibreDeck actions.
"""

# Imports ===============================================================#

import os
import platform

#--- GTK ----------------------------------------------

import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk

#--- LXML ---------------------------------------------

import lxml.etree as ET

#--- LibreDeck ----------------------------------------

# Basic handlers, mixins and abstract action classes

# import libredeck.handlers as ldhandlers
import libredeck.mixins as ldmixins
import libredeck.abstactions as ldabst

# Actions

import libredeck.web as ldweb
import libredeck.script as ldscript
import libredeck.pulseeffects as ldpaeffects
import libredeck.soundactions as ldsoundactions
import libredeck.soundplayer as ldsoundplayer

# Common utilities

# from libredeck.common import fromxml_yesno,toxml_yesno,onlyaudio,onlyimages,\
                                # cleanedbasename

from libredeck.common import onlyaudio,onlyimages,cleanedbasename

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Import actions functions ==============================================#

def import_from_folder(application):
    dialog = Gtk.FileChooserDialog(
        title="Choisissez un dossier",
        parent=application.win,
        action=Gtk.FileChooserAction.SELECT_FOLDER
    )
    dialog.add_buttons(
        Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
        Gtk.STOCK_OPEN, Gtk.ResponseType.OK,
    )

    response = dialog.run()

    if response == Gtk.ResponseType.OK:
        filepath = dialog.get_filename()
        dialog.destroy()

        return True, filepath

    if response == Gtk.ResponseType.CANCEL:
        dialog.destroy()

        return False, False

def import_soundfolder_button(application, deck):
    """
    :type application: ldeck.LibreDeck
    :type deck: libredeck.toplevel.DeckHandler
    :rtype: bool
    """

    success, folder = import_from_folder(application)

    if success:
        audio = [
            folder + os.sep + f for f in os.listdir(folder)
            if onlyaudio(f)
        ]

        if audio:

            images = [
                folder + os.sep + f for f in os.listdir(folder)
                if onlyimages(f)
            ]

            cover_matching = []

            # Finds images having the same slug as the audio files so we can 
            # use them as button images automaticaly.

            if images:

                for audio_file in audio:
                    image_matching = False

                    audio_slug = os.path.splitext(
                        os.path.basename(audio_file)
                    )[0]

                    for image_file in images:
                        img_slug = os.path.splitext(
                            os.path.basename(image_file)
                        )[0]

                        if img_slug == audio_slug:
                            image_matching = image_file
                            break

                    if image_matching:
                        cover_matching.append([audio_file, image_matching])

            for audio_file in audio:
                #---------------------------------------------------------

                new_action = ldsoundactions.SoundButtonAction(application, deck)

                new_action.sound_source = "local"
                new_action.sound_uri = audio_file

                new_action.action_configured = True

                #--- Action UI -------------------------------------------

                new_action.init_gui()
                new_action.set_title(cleanedbasename(audio_file))
                new_action.widget.guid = deck.new_action_id()

                # Apply the matching image to the button if there is one,
                # sets a default button label

                if cover_matching:
                    image = [
                        match[1] for match in cover_matching
                        if match[0] == audio_file
                    ]

                    if image:
                        new_action.set_button_image(image[0])
                        new_action.set_mode("image")
                    else:
                        new_action.set_button_label("Play")

                #---------------------------------------------------------

                deck.actions.append(new_action)

    return True

def import_soundfolder_switch(application, deck):
    """
    :type application: ldeck.LibreDeck
    :type deck: libredeck.toplevel.DeckHandler
    :rtype: bool
    """

    success, folder = import_from_folder(application)

    if success:
        audio = [
            folder + os.sep + f for f in os.listdir(folder)
            if onlyaudio(f)
        ]

        for audio_file in audio:
            #---------------------------------------------------------------

            new_action = ldsoundactions.SoundSwitchAction(application, deck)

            new_action.sound_source = "local"
            new_action.sound_uri = audio_file

            new_action.action_configured = True

            #--- Action UI -------------------------------------------------

            new_action.init_gui()
            new_action.set_title(cleanedbasename(audio_file))
            new_action.widget.guid = deck.new_action_id()

            #---------------------------------------------------------------

            deck.actions.append(new_action)

    return True

def import_frakaso(application, deck):
    """
    :type application: ldeck.LibreDeck
    :type deck: libredeck.toplevel.DeckHandler
    :rtype: bool
    """

    def read_action(xml):
        action = False

        action_title = {
            "title": False, "italic": False, "bold": False, "color": False
        }
        action_image = False
        action_class = None

        if xml.tag == "action":

            if (action_type := xml.get("type")) is not None:

                if action_type == "sound":
                    action_class = SoundButtonAction
                if action_type == "url":
                    action_class = ldweb.WebAction

                if action_class is None:
                    return []
                else:
                    action = action_class(application, deck)

                for element in xml:

                    if element.tag == "name":
                        action_title["title"] = element.text.strip()

                        if (is_bold := element.get("bold")) is not None:
                            action_title["bold"] = True

                        if (is_ital := element.get("italic")) is not None:
                            action_title["italic"] = True

                        if (color := element.get("color")) is not None:
                            action_title["color"] = color.strip()

                    if element.tag == "image":
                        action_image = element.text.strip()

                    if element.tag == "url" and action_type == "url":

                        if element.text.strip():
                            action.datas["weburl"] = element.text.strip()
                            action.action_configured = True
                        else:
                            return []

                    if element.tag == "sound" and action_type == "sound":

                        if (sound_type := element.get("type")) is not None:
                            if sound_type == "filesystem":
                                action.sound_source = "local"
                            else:
                                action.sound_source = "distant"
                        else:
                            action.sound_source = "local"

                        if (sound_volume := element.get("volume")) is not None:
                            action.sound_volume = float(sound_volume.strip())

                        if element.text.strip():
                            action.sound_uri = element.text.strip()
                            action.action_configured = True
                        else:
                            return []

                return [action,action_title,action_image]

        return []

    frakaso_actions = []

    dialog = Gtk.FileChooserDialog(
        title="Choisissez un tableau de bord Frakaso",
        parent=application.win,
        action=Gtk.FileChooserAction.OPEN
    )
    dialog.add_buttons(
        Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
        Gtk.STOCK_OPEN, Gtk.ResponseType.OK,
    )

    filter_frk = Gtk.FileFilter()
    filter_frk.set_name("Tableau de bord Frakaso")
    filter_frk.add_mime_type("application/xml")
    dialog.add_filter(filter_frk)

    filter_any = Gtk.FileFilter()
    filter_any.set_name("Tous les fichiers")
    filter_any.add_pattern("*")
    dialog.add_filter(filter_any)

    response = dialog.run()

    if response == Gtk.ResponseType.OK:
        filepath = dialog.get_filename()
        dialog.destroy()

        xml = ET.parse(filepath).getroot()

        if len(xml):
            if xml.tag == "frakaso":

                for main in xml:

                    if main.tag == "board":

                        for action in main:
                            tmp_action = read_action(action)

                            if tmp_action:
                                frakaso_actions.append(tmp_action)

        for action in frakaso_actions:
            act,act_title,act_image = action

            act.init_gui()
            act.widget.guid = deck.new_action_id()

            act.set_title(
                act_title["title"], act_title["bold"], act_title["italic"],
                act_title["color"],
            )

            if act_image:
                act.set_button_image(act_image)
                act.set_mode("image")

            deck.actions.append(act)

    if response == Gtk.ResponseType.CANCEL:
        dialog.destroy()

    return True

# Functions =============================================================#

def import_actions_init(listbox):
    """
    Fills the Import acitons action dialog with all import actions methods
    available.

    :type listbox: Gtk.Listbox
    :param listbox: GTK Listbox widget to fill
    """

    importations = []

    if ldpaeffects.COMPATIBILITY[platform.system()]:
        importations.append(
            [
                "PulseEffects", "Importer vos préréglages PulseEffects.",
                ldpaeffects.import_pulseeffects
            ],
        )

    importations = importations + [
        [
            "Dossier de sons (bouton)",
            "Importe tous les fichiers son comme des actions.\n"\
            "Le bouton utilisera une image si celle-ci a un nom identique à "\
            "celui du fichier son correspondant.",
            import_soundfolder_button
        ],
        [
            "Dossier de sons (switch)",
            "Importe tous les fichiers son comme des actions. ",
            import_soundfolder_switch
        ],
        [
            "Frakaso",
            "Importe votre configuration Frakaso.",
            import_frakaso
        ]
    ]

    # TODO Plugin system
    # A hook there to allow plugins to define an import action
    # action-import-define-hook

    def hbox(w):
        tmpbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        tmpbox.pack_start(w, True, True, 0)
        return tmpbox

    for widget in listbox.get_children():
        widget.destroy()

    for importation in importations:
        widget = Gtk.ListBoxRow()

        widget.import_function = importation[2]

        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=5)

        title = Gtk.Label()
        title.set_markup("<b>{}</b>".format(importation[0]))

        desc = Gtk.TextView()
        desc.set_wrap_mode(Gtk.WrapMode.WORD)
        desc.set_editable(False)
        desc_buffer = desc.get_buffer()
        desc_buffer.insert_at_cursor(importation[1])
        desc = hbox(desc)

        box.pack_start(title, False, False, 0)
        box.pack_start(desc, False, False, 0)

        widget.add(box)
        widget.show_all()

        listbox.add(widget)

def features_dialog_init():
    compatibilities,user_system = compatibility_summary()
    have_incompatible = len(compatibilities) - len(compatible())

    features = []

    for compatibility in compatibilities:
        feature = []

        feature.append(compatibility["human"])

        if compatibility["compat"]:
            feature.append("Oui")
        else:
            feature.append("Non")

        if "detail" in compatibility:
            feature.append(compatibility["detail"])
        else:
            feature.append("")

        features.append(feature)

    return features

def add_action_dialog_init(listbox):
    """
    Fills the Add action dialog with all action types available.

    :type listbox: Gtk.Listbox
    :param listbox: GTK Listbox widget to fill
    """

    user_system = platform.system()

    action_types = [
        [ldsoundactions.SoundButtonAction, "Son (bouton)"],
        [ldsoundactions.SoundSwitchAction, "Son (switch)"],
        [ldscript.ScriptAction, "Script"],
        [ldweb.WebAction, "Page web"]
    ]

    # TODO Plugin system
    # A hook there to append action type <-> action class to action_types
    # add-action-dialog-hook

    if ldpaeffects.COMPATIBILITY[user_system]:
        action_types.append([ldpaeffects.PulseEffectsAction, "PulseEffects"])

    for widget in listbox.get_children():
        widget.destroy()

    for action_type in action_types:
        widget = Gtk.ListBoxRow()
        widget.action_class = action_type[0]

        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)

        label = Gtk.Label()
        label.set_markup(action_type[1])

        box.pack_start(label, False, False, 0)

        widget.add(box)
        widget.show_all()

        listbox.add(widget)

def compatible():
    summary,user_system = compatibility_summary()
    return [c for c in summary if c["compat"]]

def incompatible():
    summary,user_system = compatibility_summary()
    return [c for c in summary if not c["compat"]]

def compatibility_summary():
    """
    Returns a summary of each action compatibility according to actions
    modules COMPATIBILITY.

    :rtype: dict,str
    :returns: Compatibility dict and user platform string
    """

    user_system = platform.system()

    compatibilities = [
            {
                "human": "PulseEffects", "name": "pulseeffects",
                "compat": ldpaeffects.COMPATIBILITY[user_system],
                "detail": "OS : {}".format(user_system)
            },
            {
                "human": "Son", "name": "soundplayer",
                "compat": ldsoundplayer.COMPATIBILITY[user_system],
                "detail": "Player : {}".format(ldsoundplayer.SOUND_PLAYER)
            },
            {
                "human": "Son (ajustement)", "name": "customvolume",
                "compat": ldsoundplayer.ADJUST_COMPATIBILITY[user_system],
                "detail": "Player : {}".format(ldsoundplayer.SOUND_PLAYER)
            },
            {
                "human": "Son (bouton)", "name": "soundbutton",
                "compat": ldsoundactions.COMPATIBILITY[user_system]
            },
            {
                "human": "Son (Switch)", "name": "soundswitch",
                "compat": ldsoundactions.COMPATIBILITY[user_system]
            },
            {
                "human": "Script", "name": "script",
                "compat": ldscript.COMPATIBILITY[user_system]
            },
            {
                "human": "Page web", "name": "web",
                "compat": ldweb.COMPATIBILITY[user_system]
            },
    ]

    # TODO Plugin system
    # A hook there to append plugins compatibilities to compatibilities
    # compat-hook

    return compatibilities,user_system

def action_type_to_class(action_type):
    """
    Returns the correct action class from the action type defined in a
    AbstractActionHandler class action_type attribute.

    :type action_type: string
    :param action_type: Action type name
    :rtype: class,None
    :returns: Class or None if the action type is unknown
    """

    action_types = {
        "pulseeffects": ldpaeffects.PulseEffectsAction,
        "soundbutton": ldsoundactions.SoundButtonAction,
        "soundswitch": ldsoundactions.SoundSwitchAction,
        "script": ldscript.ScriptAction,
        "web": ldweb.WebAction,
    }

    # TODO Plugin system
    # A hook there to append action type <-> action class to action_types
    # type-class-hook

    if action_type in action_types:
        return action_types[action_type]
    else:
        return None

# =======================================================================#

# vim:set shiftwidth=4 softtabstop=4:
