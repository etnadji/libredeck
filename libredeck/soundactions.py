#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
Sound actions for LibreDeck.
"""

# Imports ===============================================================#

#--- GTK ----------------------------------------------

import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk

#--- LibreDeck ----------------------------------------

# Basic handlers, mixins and abstract action classes

import libredeck.mixins as ldmixins
import libredeck.abstactions as ldabst

# Variables globales ====================================================#

__author__ = "Etienne Nadji <enadji@unistra.fr>"

# Compatibility matching (against platform.system)
COMPATIBILITY = {
    "Windows": True,
    "Linux": True,
    "Darwin": True
}

# Classes ===============================================================#

class SoundButtonAction(ldabst.AbstractButtonAction, ldmixins.MixinSoundPlayer):
    """
    Action which play a sound at button press.

    :type application: ldeck.LibreDeck
    :param application: LibreDeck application
    :type deck: ldeck.DeckHandler
    :param deck: Current deck/file
    """

    def __init__(self, application, deck):
        ldabst.AbstractButtonAction.__init__(self, application, deck)
        ldmixins.MixinSoundPlayer.__init__(self)

        self.sound_source = "local"

        self.action_type = "soundbutton"

    def init_gui(self):
        ldabst.AbstractButtonAction.init_gui(self)

    def default(self):
        self.set_button_label("Play")
        self.set_title("New sound")

    def fromxml(self, xml):
        ldabst.AbstractButtonAction.fromxml(self, xml)
        ldmixins.MixinSoundPlayer.fromxml(self, xml)
        ldmixins.MixinSoundPlayer.set_configured(self)

        return True

    def toxml(self):
        xml = ldabst.AbstractButtonAction.toxml(self)
        sound = ldmixins.MixinSoundPlayer.toxml(self)
        xml.append(sound)

        return xml

    def click(self):
        ldmixins.MixinSoundPlayer.play(self)

    def prepare_config(self):
        ldabst.AbstractButtonAction.prepare_config(self)
        ldmixins.MixinActionButton.prepare_config(self)
        ldmixins.MixinSoundPlayer.prepare_config(self)

    def validate_config(self):
        ldabst.AbstractButtonAction.validate_config(self)
        ldmixins.MixinActionButton.validate_config(self)
        ldmixins.MixinSoundPlayer.validate_config(self)
        ldabst.AbstractButtonAction.close_config(self)


class SoundSwitchAction(ldabst.AbstractSwitchAction, ldmixins.MixinSoundPlayer):
    """
    Action which play a sound at switch on, and stop it at switch off.

    :type application: ldeck.LibreDeck
    :param application: LibreDeck application
    :type deck: ldeck.DeckHandler
    :param deck: Current deck/file
    """

    def __init__(self, application, deck):
        ldabst.AbstractSwitchAction.__init__(self, application, deck)
        ldmixins.MixinSoundPlayer.__init__(self)

        self.sound_source = "local"

        self.action_type = "soundswitch"

    def init_gui(self):
        ldabst.AbstractSwitchAction.init_gui(self)

    def default(self):
        self.set_title("New sound")

    def fromxml(self, xml):
        ldabst.AbstractSwitchAction.fromxml(self, xml)
        ldmixins.MixinSoundPlayer.fromxml(self, xml)
        ldmixins.MixinSoundPlayer.set_configured(self)

        return True

    def toxml(self):
        xml = ldabst.AbstractSwitchAction.toxml(self)
        sound = ldmixins.MixinSoundPlayer.toxml(self)
        xml.append(sound)

        return xml

    def prepare_config(self):
        ldabst.AbstractSwitchAction.prepare_config(self)
        ldmixins.MixinSoundPlayer.prepare_config(self)

    def validate_config(self):
        ldabst.AbstractSwitchAction.validate_config(self)
        ldmixins.MixinSoundPlayer.validate_config(self)
        ldabst.AbstractSwitchAction.close_config(self)

    def switch_off(self):
        ldmixins.MixinSoundPlayer.stop(self)

    def switch_on(self):
        ldmixins.MixinSoundPlayer.play(self)

# vim:set shiftwidth=4 softtabstop=4:
