#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
"""

# Imports ===============================================================#

import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Classes ===============================================================#

class BaseHandler:

    def __init__(self, builder=False, application=False):
        self.builder = builder
        self.application = application
        self.builder_file = False

    def _gladeobj(self, objname):
        return self.builder.get_object(objname)

    def init_builder(self):
        self.builder = Gtk.Builder()
        # self.builder.add_from_file(self.builder_file)
        self.builder.add_from_file(self.application.ui_file(self.builder_file))
        self.builder.connect_signals(self)

    def init_gui(self):

        if not self.builder:
            self.init_builder()

# vim:set shiftwidth=4 softtabstop=4:
