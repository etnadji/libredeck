#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
Script action for LibreDeck.
"""

# Imports ===============================================================#

import os
import subprocess

#--- GTK ----------------------------------------------

import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk

#--- LXML ---------------------------------------------

import lxml.etree as ET

#--- LibreDeck ----------------------------------------

import libredeck.abstactions as ldabst

from libredeck.common import fromxml_yesno,toxml_yesno

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Compatibility matching (against platform.system)
COMPATIBILITY = {
    "Windows": True,
    "Linux": True,
    "Darwin": True
}

# Classes ===============================================================#

class ScriptAction(ldabst.AbstractButtonAction):
    """
    Action which runs a script at button press.

    :type application: ldeck.LibreDeck
    :param application: LibreDeck application
    :type deck: ldeck.DeckHandler
    :param deck: Current deck/file
    """

    def __init__(self, application, deck):
        ldabst.AbstractButtonAction.__init__(self, application, deck)
        self.datas["arguments"] = []

        self.action_type = "script"

        # self.config["file"] = "ui/script.glade"
        self.config["file"] = "script"

    def prepare_config(self):
        ldabst.AbstractButtonAction.prepare_config(self)

        if self.datas["arguments"]:
            main = self._gladeconfobj("main_entry")
            main.set_text(self.datas["arguments"][0])

            args = self._gladeconfobj("args_entry")

            try:
                args.set_text(" ".join(self.datas["arguments"][1:]))
            except IndexError:
                pass

        config_widget = self._gladeconfobj("config_box")
        self.add_to_config_notebook(config_widget, "Script")

    def fromxml(self, xml):
        ldabst.AbstractButtonAction.fromxml(self, xml)

        for element in xml:

            if element.tag == "arguments":
                self.datas["arguments"] = element.text.strip().split()
                self.action_configured = True

    def toxml(self):
        xml = ldabst.AbstractButtonAction.toxml(self)

        if self.datas["arguments"]:
            args = ET.Element("arguments")
            args.text = " ".join(self.datas["arguments"])
            xml.append(args)

        return xml

    def validate_config(self):
        ldabst.AbstractButtonAction.validate_config(self)

        main = self._gladeconfobj("main_entry").get_text().split()
        args = self._gladeconfobj("args_entry").get_text().split()

        new_args = main + args

        if new_args != self.datas["arguments"]:
            self.datas["arguments"] = new_args
            self.action_configured = True

    def default(self):
        self.set_title("Script")
        self.set_button_label("Lancer")
        self.set_mode("text")

    def init_gui(self):
        ldabst.AbstractButtonAction.init_gui(self)

        if self.datas["arguments"]:
            self.action_configured = True

    def click(self):
        if self.action_configured:
            subprocess.Popen(self.datas["arguments"])
        else:
            self.application.non_configured_action(self)

# vim:set shiftwidth=4 softtabstop=4:
