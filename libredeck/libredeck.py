#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
LibreDeck, a virtual streamdeck for GNOME.
"""

RUN_LIBREDECK = True

# Imports ===============================================================#

import os
import sys

#--- appdirs ------------------------------------------

try:
    from appdirs import user_config_dir
except ModuleNotFoundError:
    print("- LibreDeck requires appdirs. Please install it.")

#--- LibreDeck ----------------------------------------

import libredeck.config as ldconfig
import libredeck.app as ldapp

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Programme =============================================================#

def app(arguments):
    """
    Initialize folders, configuration and runs LibreDeck

    :type arguments: list
    :param arguments: Command line arguments (sys.argv)
    :returns: Standard return code
    :rtype: int
    """

    debug = "--debug" in arguments

    #--- Configuration ------------------------------------

    EXEC_PATH = os.path.realpath(__file__)
    UI_PATH = os.sep.join(EXEC_PATH.split(os.sep)[:-2] + ["ui"])

    if os.name == "nt":
        # Skipping the application author folder used on Windows
        # => /USER/AppData/Local/LibreDeck
        CONFIG_DIR = os.sep.join([user_config_dir(), "LibreDeck"])
    else:
        # Usually ~/.config/LibreDeck/
        CONFIG_DIR = user_config_dir("LibreDeck")

    if debug:
        print("Exec path :", EXEC_PATH)
        print("UI path :", UI_PATH)
        print("Config folder :", CONFIG_DIR)

    if not os.path.exists(CONFIG_DIR):
        os.makedirs(CONFIG_DIR)

    CONFIG_FILE = f"{CONFIG_DIR}{os.sep}settings.xml"

    if debug:
        print("Config file :", CONFIG_FILE)

    config = ldconfig.Settings(CONFIG_FILE)

    if os.path.exists(CONFIG_FILE):
        # Just read the config file or...
        config.fromfile()
    else:
        if debug:
            print("No config file found. Creating a new one.")

        # ... Creates it at first startup
        config.tofile()

    #--- Load GUI and setup handler -----------------------

    ldeck = ldapp.LibreDeck(config, UI_PATH, arguments)
    ldeck.init_gui()

    #--- Running GUI --------------------------------------

    ldeck.show_all()

    #--- End ----------------------------------------------

    return 0

def main():
    """Runs LibreDeck"""

    code = 1

    if RUN_LIBREDECK:
        if ldapp.RUN_LIBREDECK:
            code = app(sys.argv)

    sys.exit(code)

# vim:set shiftwidth=4 softtabstop=4:
