#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
LibreDeck main window / application
"""

# =======================================================================#

RUN_LIBREDECK = True

# Imports ===============================================================#

import os
import subprocess
import webbrowser

import urllib
import urllib.request

#--- GTK ----------------------------------------------

try:
    import gi

    gi.require_version('Gtk', '3.0')

    from gi.repository import Gtk

except ModuleNotFoundError:
    print("- LibreDeck requires PyGObject. Please install it.")
    RUN_LIBREDECK = False

#--- LXML ---------------------------------------------

try:
    import lxml.etree as ET

except ModuleNotFoundError:
    print("- Frakaso requires LXML. Please install it.")
    RUN_LIBREDECK = False

#--- LibreDeck ----------------------------------------

import libredeck.toplevel as ldtoplevel
import libredeck.actions as ldactions
import libredeck.soundplayer as ldsoundplayer

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"
__version__ = 0.2

# Classes ===============================================================#

class LibreDeck:
    """
    LibreDeck application main class.

    :type settings: libredeck.config.Settings
    :param settings: LibreDeck settings object
    :type ui_folder: str
    :param ui_folder: Path of UI files
    :type arguments: list
    :param arguments: Command line arguments (sys.argv)
    """

    def __init__(self, settings, ui_folder, arguments=[]):
        # GTK UI files
        self.ui_folder = ui_folder

        # GTK builder instance
        self.builder = Gtk.Builder()
        self.builder.add_from_file(self.ui_file("gui"))
        self.builder.connect_signals(self)

        # GTK settings
        self.gtk_settings = Gtk.Settings.get_default()

        self.settings = settings

        # NOTE Absolutely repetitive but absolutely correct anyway
        self.soundplayer = ldsoundplayer.SoundPlayer(
            ldsoundplayer.SOUND_PLAYER
        )

        # Decks files and welcome page
        self.decks = []
        self.welcome_page = False

        # Dialogs
        self.import_dialog = False
        self.open_dialog = False
        self.save_dialog = False
        self.options_dialog = False

        self.arguments = arguments

    def _debug(self):
        return "--debug" in self.arguments

    def _gladeobj(self, objname):
        return self.builder.get_object(objname)

    def ui_file(self, ui_file):
        return os.sep.join([self.ui_folder, ui_file + ".glade"])

    def non_implemented(self):
        """
        Show a message dialog to warn the user that the feature is not
        implemented yet.
        """

        dialog = Gtk.MessageDialog(
            transient_for=self.win,
            message_type=Gtk.MessageType.WARNING,
            buttons=Gtk.ButtonsType.OK,
            text="Fonctionnalité non implémentée",
        )

        dialog.format_secondary_text(
            "Cette fonctionnalité n’est pas encore disponible."
        )

        dialog.run()

        dialog.destroy()

    def non_configured_action(self, action):
        """
        Show a message dialog when the action is not configured [enough].

        :type action: libredeck.actions.AbstractActionHandler
        :param action: Action being not configured [enough]
        """

        dialog = Gtk.MessageDialog(
            transient_for=self.win,
            message_type=Gtk.MessageType.WARNING,
            buttons=Gtk.ButtonsType.OK,
            text="Action non configurée",
        )

        dialog.format_secondary_text(
            "Cette action n’est pas (assez) configurée pour être lancée."
        )

        dialog.run()

        dialog.destroy()

    def stop(self):
        """Stop all sounds playing."""
        self.soundplayer.stop()

    def onDestroy(self, *args):
        Gtk.main_quit()

    #=== GUI init and update =============================================

    def init_gui(self):

        def _compatibilities():
            compatibilities,user_system = ldactions.compatibility_summary()
            compatible = len(ldactions.compatible())
            have_incompatible = len(compatibilities) - compatible

            if have_incompatible:
                print(
                    f"Found action(s) incompatible(s) to {user_system}"\
                    " system:"
                )
                print(
                    ",".join(
                        [c["name"] for c in ldactions.incompatible()]
                    )
                )
            else:
                print("All actions compatible with the current system.")

        #--- Dark theme ---------------------------------------------

        self.gtk_settings.set_property(
            "gtk-application-prefer-dark-theme",
            self.settings.ui["dark_theme"]
        )

        #--- App main window ----------------------------------------

        self.win = self._gladeobj("appWindow")

        #--- Files/Deck/Welcome switch ------------------------------

        self.files_stack = self._gladeobj("filesStackView")

        self.files_stack.connect(
            "notify::visible-child", self.at_filestack_changed
        )

        #------------------------------------------------------------

        self.update_on_selected_action()

        #--- Keyboard shortcuts -------------------------------------

        self.accelerators = Gtk.AccelGroup()
        self.win.add_accel_group(self.accelerators)

        #--- Debug compatibilities ----------------------------------

        if self._debug():
            _compatibilities()

        #--- Load deck file in arguments or welcome page ------------

        do_welcome = True

        if self.arguments:

            if self.arguments[-1].endswith(".xml"):
                filepath = os.path.realpath(self.arguments[-1])

                if os.path.exists(filepath):
                    self.settings.add_recent(filepath)
                    do_welcome = False
                    self.add_deck(filepath)

            if do_welcome:
                if self.settings.recent:
                    if self.settings.ux["deck_startup"]:
                        self.add_deck(self.settings.recent[0], isrecent=True)
                        do_welcome = False

        if do_welcome:
            self.add_welcome()

        #--- New version --------------------------------------------

        if self.settings.ux["version_startup"]:
            available, version, comm, longcom = self._new_version_available()

            if available and version > __version__:
                self.show_new_version(version, comm, longcom)

        #------------------------------------------------------------

    def show_all(self):
        """Show LibreDeck main window and start GTK main loop."""

        self.update_app_bar()
        self.update_bottom_bar()

        self.win.show_all()
        Gtk.main()

    def at_filestack_changed(self, stack, param):
        """
        Callback when the filestack for deck/welcome page changes its visible
        child.

        Update LibreDeck application and bottom bar.
        """
        self.update_app_bar()
        self.update_bottom_bar()

    def update_bottom_bar(self):
        """Update the UI of the bottom bar."""

        current_deck,is_deck = self._current_deck()
        deck_cols_label = self._gladeobj("deckColumsLabel")

        if is_deck:
            for w in ["deckColumsLabel"]:
                self._gladeobj(w).set_visible(True)

            deck_cols = self._gladeobj("deckColumnsButton")
            deck_cols.set_visible(True)
            deck_cols.set_value(current_deck.box.get_max_children_per_line())
        else:
            for w in ["deckColumnsButton", "deckColumsLabel"]:
                self._gladeobj(w).set_visible(False)

    def update_app_bar(self):
        """Update the UI of LibreDeck application bar."""

        current_name = self._current_file_name()

        # If the current file/deck/page view is not a deck, disable the
        # sensitivity of action-related buttons.

        buttons = ["stopActionsButton", "addActionButton"]

        if current_name is None or current_name == "libredeck-welcome":
            # NOTE None At LibreDeck startup

            for widget in buttons:
                self._gladeobj(widget).set_sensitive(False)

            self.update_on_selected_action()
        else:
            for widget in buttons:
                self._gladeobj(widget).set_sensitive(True)

    def update_on_selected_action(self, selected=False):
        """
        Update the UI of LibreDeck application bar when an action is selected
        by the user.
        """

        buttons = ["optionsActionButton", "deleteActionButton"]

        for widget in buttons:
            self._gladeobj(widget).set_sensitive(selected)

    #=== Decks management ================================================

    def _current_deck(self):
        """
        Returns the current deck object (≠ deck widget).

        :rtype: object, bool
        :returns: object, bool if deck; object, False if welcome page; None,
            False if any.
        """

        current_name = self._current_file_name()

        if current_name == "libredeck-welcome":
            return self.welcome_page,False
        else:
            for deck in self.decks:
                if deck.deck_id == current_name:
                    return deck,True

        return None,False

    def _current_deck_action(self):
        current_deck,is_deck = self._current_deck()

        if is_deck:
            action,aid,abox = current_deck.get_selected_action()

            if action:
                return action,aid,abox

        return False, False, False

    def _current_file_name(self):
        return self.files_stack.get_visible_child_name()

    def open_deck(self):
        """Show the open deck dialog"""

        if not self.open_dialog:

            self.open_dialog = self._gladeobj("openBoardFileDialog")
            self.open_dialog.set_transient_for(self.win)

            filter_ld = Gtk.FileFilter()
            filter_ld.set_name("LibreDeck (*.xml)")
            filter_ld.add_pattern("*.xml")
            self.open_dialog.add_filter(filter_ld)

            filter_any = Gtk.FileFilter()
            filter_any.set_name("Tous les fichiers")
            filter_any.add_pattern("*")
            self.open_dialog.add_filter(filter_any)

            self.open_dialog.set_filter(filter_ld)

        self.open_dialog.show()

    def new_deck(self):
        """Add a new empty deck"""

        self.add_deck(None)

    def save_deck(self):
        """Save the current deck."""

        self.settings.tofile()

        deck,is_deck = self._current_deck()

        if is_deck and deck.filepath:
            # Already existing deck : just save the deck

            with open(deck.filepath, "w", encoding="utf8") as xf:
                xf.write(deck.toxmlstr())

            return True
        else:
            # New deck : choose a deck title and a filepath
            self.open_deck_title_dialog()

        return False

    def close_current(self):
        """
        Close the current deck / welcome page.
        """

        current_name = self._current_file_name()
        close = False

        if current_name is not None:
            if current_name == "libredeck-welcome":
                if len(self.decks) >= 1:
                    close = True
                    self.welcome_page = False
            else:
                if self.welcome_page:
                    close = True
                else:
                    if len(self.decks) != 1:
                        close = True

            if close:
                self.files_stack.get_child_by_name(current_name).destroy()

    def add_welcome(self):
        """Add the welcome page."""

        welcome = ldtoplevel.WelcomeHandler(self)
        welcome.init_gui()

        self.welcome_page = welcome

    def add_deck(self, filepath=None, isrecent=False):
        """
        Add a new deck from the file filepath.

        :type filepath: string,None
        :param filepath: Deck's filepath or None
        :type isrecent: boolean
        :param isrecent: Don't add the filepath to the recent files list. Only
            use this if filepath is already the last recent file...
        """

        if self._debug() and filepath is not None:
            print(f"Opening {filepath}")

        deck = ldtoplevel.DeckHandler(self, self.new_deck_id(), filepath)
        deck.init_gui()

        stack = self._gladeobj("filesStackView")
        stack.add_titled(deck.widget, deck.deck_id, deck.title)

        self.decks.append(deck)

        if filepath is not None:
            if not isrecent:
                self.settings.add_recent(filepath)

        self.files_stack.set_visible_child(deck.widget)

    def new_deck_id(self):
        """Returns a new deck ID."""

        return len(self.decks) + 1

    def open_save_dialog(self):
        """Open the save deck dialog."""

        if not self.save_dialog:

            self.save_dialog = self._gladeobj("saveBoardFileDialog")
            self.save_dialog.set_transient_for(self.win)

            filter_ld = Gtk.FileFilter()
            filter_ld.set_name("LibreDeck (*.xml)")
            filter_ld.add_pattern("*.xml")
            self.save_dialog.add_filter(filter_ld)

            filter_any = Gtk.FileFilter()
            filter_any.set_name("Tous les fichiers")
            filter_any.add_pattern("*")
            self.save_dialog.add_filter(filter_any)

            self.save_dialog.set_filter(filter_ld)

        self.save_dialog.show()

    def open_deck_title_dialog(self):
        """Open the deck title dialog."""

        self.decktitle_dialog = self._gladeobj("newBoardTitleDialog")
        self.decktitle_dialog.set_transient_for(self.win)
        self.decktitle_dialog.show()

    def on_od_deckfolder_open_clicked(self, button):
        deck,is_deck = self._current_deck()

        if is_deck:
            folder = os.path.dirname(os.path.realpath(deck.filepath))

            if self.settings.run["system"] == "Windows":
                subprocess.Popen(f'explorer {os.path.realpath(folder)}')
            else:
                subprocess.Popen(["xdg-open", os.path.realpath(folder)])

    def on_od_deckfile_open_clicked(self, button):
        deck,is_deck = self._current_deck()

        if is_deck:
            if self.settings.run["system"] == "Windows":
                subprocess.Popen(f'notepad {os.path.realpath(deck.filepath)}')
            else:
                subprocess.Popen(["xdg-open", os.path.realpath(deck.filepath)])

    def on_sbfd_validate_clicked(self, button):
        """Callback for save deck dialog : validate button."""

        deck,is_deck = self._current_deck()

        if is_deck:
            new_file = self.save_dialog.get_filename()

            if not new_file.endswith(".xml"):
                new_file += ".xml"

            if new_file:
                deck.filepath = new_file
                self.save_deck()

                # Destroy the current deck in the view, and reopen it to
                # update the stack title, as we can't change a stack item
                # title...

                self.files_stack.get_child_by_name(
                    self.files_stack.get_visible_child_name()
                ).destroy()

                self.add_deck(new_file)

        self.save_dialog.hide()

    def on_sbfd_cancel_clicked(self, button):
        """Callback for save deck dialog : cancel button."""
        self.save_dialog.hide()

    def on_obfd_validate_clicked(self, button):
        """Callback for open deck dialog : validate button."""

        deck_file = self.open_dialog.get_filename()

        if deck_file:
            self.add_deck(deck_file)

        self.open_dialog.hide()

    def on_obfd_cancel_clicked(self, button):
        """Callback for open deck dialog : cancel button."""
        self.open_dialog.hide()

    def on_nbt_validate_clicked(self, button):
        """Callback for new deck title dialog : validate button."""

        deck,is_deck = self._current_deck()

        title = self._gladeobj("nbt_entry").get_text()

        self.decktitle_dialog.hide()

        if is_deck and title:
            deck.title = title
            self.open_save_dialog()

    def on_nbt_cancel_clicked(self, button):
        """Callback for new deck title dialog : cancel button."""

        self.decktitle_dialog.hide()

    #=== Buttons =========================================================

    def on_appMenuNew_activate(self, button):
        """
        Application menu callback : creates an empty deck.

        ..sealso : libredeck.app.LibreDeck.new_deck
        """
        self.new_deck()

    def on_appFeatures_activate(self, event):
        self.features_dialog = self._gladeobj("featuresNewDialog")
        self.features_dialog.set_transient_for(self.win)

        box = self._gladeobj("featuresBox")

        for widget in box.get_children():
            widget.destroy()

        featurestore = Gtk.ListStore(str, str, str)

        for feature in ldactions.features_dialog_init():
            featurestore.append(feature)

        featurefilter = featurestore.filter_new()

        treeview = Gtk.TreeView.new_with_model(featurefilter)

        for i, column_title in enumerate(
                ["Fonctionnalité", "OK", "Détails"]):
                renderer = Gtk.CellRendererText()
                column = Gtk.TreeViewColumn(column_title, renderer, text=i)
                treeview.append_column(column)

        treeview.show_all()
        box.pack_start(treeview, True, True, 0)
        self.features_dialog.run()

    def on_featuresOkButton_clicked(self, button):
        self.features_dialog.hide()

    def on_stopActionsButton_clicked(self, button):
        """
        Application bar button callback : stop all actions sounds.

        ..sealso : libredeck.app.LibreDeck.stop
        """
        self.stop()

    def on_addActionButton_clicked(self, button):
        deck,is_deck = self._current_deck()

        if is_deck:
            self.add_action_dialog = self._gladeobj("addActionDialog")
            self.add_action_dialog.set_transient_for(self.win)

            listbox = self._gladeobj("adda_actionsListBox")

            ldactions.add_action_dialog_init(listbox)

            self.add_action_dialog.run()

    def on_adda_cancelButton_clicked(self, button):
        self.add_action_dialog.hide()

    def on_adda_validateButton_clicked(self, button):
        new_action = False

        deck,is_deck = self._current_deck()

        if is_deck:
            listbox = self._gladeobj("adda_actionsListBox")

            selection = [r.action_class for r in listbox.get_selected_rows()]

            if selection:
                action_class = selection[0]

                action = action_class(self, deck)
                action.init_gui()
                action.widget.guid = deck.new_action_id()
                action.default()
                deck.actions.append(action)

        self.add_action_dialog.hide()

        if action:
            action.show_config()

    def on_optionsActionButton_clicked(self, button):
        action,aid,abox = self._current_deck_action()

        if action:
            action.show_config()

    def on_deleteActionButton_clicked(self, button):
        action,aid,abox = self._current_deck_action()

        if action:
            abox.destroy()

    def on_appMenuClose_activate(self, event):
        """
        Application menu callback : close the current deck.

        ..sealso : libredeck.app.LibreDeck.close_current
        """
        self.close_current()

    def on_appMenuQuit_activate(self, event):
        """
        Application menu callback : quit LibreDeck.

        ..sealso : libredeck.app.LibreDeck.onDestroy
        """
        self.onDestroy()

    def on_appAbout_activate(self, event):
        """
        Application menu callback : show about dialog.

        ..sealso : libredeck.app.LibreDeck.about
        """
        self.about()

    def on_appMenuOpen_activate(self, button):
        """
        Application menu callback : open a deck.

        ..sealso : libredeck.app.LibreDeck.open_deck
        """
        self.open_deck()

    def on_appSave_activate(self, button):
        """
        Application menu callback : save the current file.

        ..sealso : libredeck.app.LibreDeck.save_deck
        """
        success = self.save_deck()

    def on_appImport_activate(self, button):
        """
        Application menu callback : import multiple actions.

        ..sealso : libredeck.app.LibreDeck.import_dialog
        """
        self.show_import_dialog()

    def on_appOptions_activate(self, button):
        """
        Application menu callback : show options dialog.

        ..sealso : libredeck.app.LibreDeck.show_options
        """
        self.show_options()

    #=== Bottom bar ======================================================

    def on_deckColumnsButton_value_changed(self, button):
        """
        Callback for bottom bar column customizer.

        Change the max action columns for the current deck.

        ..sealso : libredeck.app.LibreDeck.show_options
        """
        deck,is_deck = self._current_deck()

        if is_deck:
            cols = button.get_value_as_int()
            deck.box.set_max_children_per_line(cols)

    #=== New option dialog ===============================================

    def show_new_version(self, new_ver, new_com="", new_long=""):
        self.newversion_dialog = self._gladeobj("newVersionDialog")
        self.newversion_dialog.set_transient_for(self.win)

        #--- Link button ----------------------------------

        base_url = "https://etnadji.fr/rsc/libredeck"

        button = self._gladeobj("newVersionLink")

        if self.settings.run["system"] == "Windows":
            button.set_uri(f"{base_url}/win.html")
            button.set_label("LibreDeck pour Windows")
        elif self.settings.run["system"] == "Linux":
            button.set_uri(f"{base_url}/gnu.html")
            button.set_label("LibreDeck pour GNU/Linux")
        # elif self.settings.run["system"] == "Darwin":
            # pass
        else:
            button.set_uri(f"{base_url}/")
            button.set_label("Site web de LibreDeck")

        #--- Changelog notes ------------------------------

        longbuffer = self._gladeobj("newVersionBuffer")

        if new_long:
            longbuffer.set_text(new_long)
        else:
            longbuffer.set_text(new_com)

        #--- Show the dialog ------------------------------

        self.newversion_dialog.show()

    def on_validateNewVersion_clicked(self, button):
        self.newversion_dialog.hide()

    #=== Options dialog ==================================================

    def show_options(self):
        #-------------------------------------------------------

        if not self.options_dialog:
            self.options_dialog = self._gladeobj("optionsDialog")
            self.options_dialog.set_transient_for(self.win)

        #--- Deck options --------------------------------------

        deck,is_deck = self._current_deck()

        deck_title = self._gladeobj("od_decktitle_entry")

        if is_deck:
            deck_title.set_text(deck.title)
            deck_title.set_sensitive(True)
        else:
            deck_title.set_text("")
            deck_title.set_sensitive(False)

        #-------------------------------------------------------

        for setting in [
            ["od_theme_switch", self.settings.ui["dark_theme"]],
            ["od_button_title_switch", self.settings.ui["hide_button_title"]],
            ["od_lastdeckStartup_switch", self.settings.ux["deck_startup"]],
            ["od_newver_switch", self.settings.ux["version_startup"]]]:
            widget = self._gladeobj(setting[0])
            widget.set_state(setting[1])

        if self.settings.ux["browser"]:
            browser = self._gladeobj("od_browser_button")

            fn = self.settings.ux["browser"]

            if fn.endswith(" %s"):
                fn = fn.split()[-2]

            browser.set_filename(fn)

        #-------------------------------------------------------

        self.options_dialog.show()

    def on_od_validate_clicked(self, button):
        self.settings.tofile()
        self.options_dialog.hide()

    def on_od_lastdeckStartup_switch_state_set(self, switch, state):
        self.settings.ux["deck_startup"] = state

    def on_od_theme_switch_state_set(self, switch, state):
        self.gtk_settings.set_property(
            "gtk-application-prefer-dark-theme", state
        )

        self.settings.ui["dark_theme"] = state

    def on_od_button_title_switch_state_set(self, switch, state):
        self.settings.ui["hide_button_title"] = state

    def on_od_newver_switch_state_set(self, switch, state):
        self.settings.ux["version_startup"] = state

    def on_od_browser_button_file_set(self, button):
        filepath = button.get_filename()
        filepath += " %s"

        self.settings.ux["browser"] = filepath
        self.settings.set_browser()

    #=== Import actions dialog ===========================================

    def show_import_dialog(self):
        deck,is_deck = self._current_deck()

        if is_deck:

            if not self.import_dialog:
                self.import_dialog = self._gladeobj("importDialog")
                self.import_dialog.set_transient_for(self.win)

                listbox = self._gladeobj("iad_listbox")
                ldactions.import_actions_init(listbox)

            self.import_dialog.run()

    def on_iad_validate_clicked(self, button):
        deck,is_deck = self._current_deck()

        if is_deck:
            listbox = self._gladeobj("iad_listbox")
            selected = listbox.get_selected_rows()

            if selected:
                import_function = selected[0].import_function
                success = import_function(self, deck)

            self.import_dialog.hide()

    def on_iad_cancel_clicked(self, button):
        deck,is_deck = self._current_deck()

        if is_deck:
            self.import_dialog.hide()

    #=== About dialog ====================================================

    def _new_version_available(self):
        """
        Check if a new version of LibreDeck is available.
        """

        remote_log = 'https://framagit.org/etnadji/libredeck/-/raw/master/'
        remote_log += 'install/changelog.xml'

        remote_version = __version__
        remote_comment = ""
        remote_long = ""

        if self._debug():
            print("Checking new version against", __version__)

        try:

            with urllib.request.urlopen(remote_log) as response:
                code = response.read()
                xml = ET.fromstring(code)

                for main in xml:

                    if main.tag == "log":

                        version = main.get("version")
                        comment = main.get("comments")
                        long_comment = main.get("long")

                        if version is not None:
                            remote_version = float(version.strip())

                        if comment is None:
                            remote_comment = ""
                        else:
                            remote_comment = comment.strip()

                        if long_comment is None:
                            remote_long = ""
                        else:
                            remote_long = long_comment.strip()

                        break

        except urllib.error.HTTPError:
            # Network errors
            pass

        except ET.XMLSyntaxError:
            # If XML file is not available / other
            pass

        if remote_version > __version__:
            if self._debug():
                print("=> Found new version :", remote_version)

            return True, remote_version, remote_comment, remote_long
        else:
            return False, remote_version, remote_comment, remote_long

    def about(self):
        """Show the About LibreDeck dialog."""

        #=== Checking new versions ==================================

        available, version, comment, longcom = self._new_version_available()

        # Setting the version label of the About dialog

        version_label = str(__version__)

        if available and version > __version__:

            version_label += f" (new version available : {version})"

            if comment:
                version_label += f"\n{comment}"

        #=== About dialog ===========================================

        about = self._gladeobj("aboutDialog")
        about.set_transient_for(self.win)

        about.set_version(version_label)

        comments = about.get_comments()
        about.set_comments(
            "\n".join(
                [comments]
            )
        )

        about.run()
        about.hide()

# vim:set shiftwidth=4 softtabstop=4:
