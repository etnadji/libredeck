#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
PulseEffects action and mass-importer for LibreDeck.
"""

# Imports ===============================================================#

import os
import subprocess

#--- GTK ----------------------------------------------

import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk

#--- LXML ---------------------------------------------

import lxml.etree as ET

#--- LibreDeck ----------------------------------------

import libredeck.abstactions as ldabst

from libredeck.common import fromxml_yesno,toxml_yesno

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Compatibility matching (against platform.system)
COMPATIBILITY = {
    "Windows": False,
    "Linux": True,
    "Darwin": False
}

# Classes ===============================================================#

class PulseEffectsAction(ldabst.AbstractSwitchAction):
    """
    Action which activate/desactivate a PulseEffect preset.

    :type application: ldeck.LibreDeck
    :param application: LibreDeck application
    :type deck: ldeck.DeckHandler
    :param deck: Current deck/file
    """

    def __init__(self, application, deck):
        super().__init__(application, deck)
        self.action_type = "pulseeffects"

        # self.builder_file = "ui/pulseeffects.glade"
        self.builder_file = "pulseeffects"

        # self.config["file"] = "ui/pulseeffects.glade"
        self.config["file"] = "pulseeffects"
        self.datas["preset"] = False
        self.datas["allpresets"] = {}

    def fromxml(self, xml):
        ldabst.AbstractSwitchAction.fromxml(self, xml)

        for element in xml:

            if element.tag == "preset":
                self.datas["preset"] = element.text.strip()
                self.action_configured = True

    def toxml(self):
        xml = ldabst.AbstractSwitchAction.toxml(self)

        preset = ET.Element("preset")

        if self.datas["preset"]:
            preset.text = self.datas["preset"]

        xml.append(preset)

        return xml

    def default(self):
        self.set_title("New PulseEffect preset")

    def presets(self):

        def get_preset_list(s, title):
            s = s.split(title)[-1]
            s = [ii for ii in [i.strip() for i in s.split(",")] if ii]
            return s

        raw = subprocess.getoutput("pulseeffects -p")

        outp,inp = raw.split("\n")
        inp = get_preset_list(inp, "Input Presets: ")
        outp = get_preset_list(outp, "Output Presets: ")

        return inp, outp

    def own_preset_category(self):
        return self.preset_category(self.datas["preset"])

    def preset_category(self, preset_name):
        inp,outp = self.presets()

        if preset_name in outp:
            return "output"
        elif preset_name in inp:
            return "input"
        else:
            return False

    def prepare_config(self):
        ldabst.AbstractSwitchAction.prepare_config(self)

        # Get all PulseEffects presets from command line ----------------------

        inp,outp = self.presets()

        # Reset and fill the preset combobox ----------------------------------

        combobox = self._gladeconfobj("presetsComboBox")

        combobox.remove_all()

        # Fill the combobox

        combobox.append("None", "Aucun")

        pid = 0

        self.datas["allpresets"] = {}

        for ptype in [[sorted(outp), "Sortie"], [sorted(inp), "Entrée"]]:

            for preset in ptype[0]:
                pid += 1
                label = "{} – {}".format(ptype[1], preset)

                combobox.append(str(pid), label)
                self.datas["allpresets"][str(pid)] = preset

        # Set the curent item of the combobox

        cid = "None"

        if self.datas["preset"]:
            for pid,pr in self.datas["allpresets"].items():
                if pr == self.datas["preset"]:
                    cid = pid
                    break

        combobox.set_active_id(cid)

        # Add the PulseEffects tab --------------------------------------------

        config_widget = self._gladeconfobj("configtab")

        self.add_to_config_notebook(config_widget, "PulseEffects")

        # ---------------------------------------------------------------------

    def cancel_config(self):
        ldabst.AbstractSwitchAction.cancel_config(self)
        ldabst.AbstractSwitchAction.close_config(self)

    def validate_config(self):
        ldabst.AbstractSwitchAction.validate_config(self)

        # Get the PulseEffects preset and set it

        cid = self._gladeconfobj("presetsComboBox").get_active_id()

        try:
            if self.datas["allpresets"][cid] != self.datas["preset"]:
                self.datas["preset"] = self.datas["allpresets"][cid]
                self.update_icon()
        except KeyError:
            self.datas["preset"] = False

        ldabst.AbstractSwitchAction.close_config(self)

    def init_gui(self):
        ldabst.AbstractSwitchAction.init_gui(self)

        if self.datas["preset"]:
            self.set_title(self.datas["preset"])
            self.update_icon()

        else:
            self.set_title("PulseEffects", italic=True)

    def update_icon(self):
        category = self.own_preset_category()

        if category:
            if category == "input":
                self.icon_input()
            if category == "output":
                self.icon_output()

    def icon_input(self):
        self._gladeobj("pea_icon").set_from_icon_name(
            "audio-input-microphone", Gtk.IconSize.BUTTON
        )

    def icon_output(self):
        self._gladeobj("pea_icon").set_from_icon_name(
            "audio-volume-high", Gtk.IconSize.BUTTON
        )

    def switch_on(self):
        if self.datas["preset"]:
            print(self.datas["preset"])
            cmd = ["pulseeffects", "--load-preset", self.datas["preset"]]
            subprocess.Popen(cmd)
        else:
            self.application.non_configured_action(self)
            self.set_state(False)

    def switch_off(self):
        if self.datas["preset"]:
            cmd = ["pulseeffects", "--reset"]
            subprocess.Popen(cmd)

# Fonctions =============================================================#

def import_pulseeffects(application, deck):
    """
    Import all PulseEffects presets into the current deck.

    :type application: ldeck.LibreDeck
    :type deck: libredeck.toplevel.DeckHandler
    :rtype: bool
    """

    def get_preset_list(s, title):
        s = s.split(title)[-1]
        s = [ii for ii in [i.strip() for i in s.split(",")] if ii]
        return s

    raw = subprocess.getoutput("pulseeffects -p")

    outp,inp = raw.split("\n")
    inp = sorted(get_preset_list(inp, "Input Presets: "))
    outp = sorted(get_preset_list(outp, "Output Presets: "))

    for ptype in [outp, inp]:
        for preset in ptype:
            new_action = PulseEffectsAction(application, deck)
            new_action.datas["preset"] = preset
            new_action.init_gui()
            new_action.widget.guid = deck.new_action_id()
            deck.actions.append(new_action)

    return True

# vim:set shiftwidth=4 softtabstop=4:
