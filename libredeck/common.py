#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
Common functions for some LibreDeck modules.
"""

# Imports ===============================================================#

import os

# Variables globales ====================================================#

GNULINUX_AUDIO = [
    "aac", "mp3", "mp4", "ogg", "ogv", "opus", "wav", "midi"
]

# Sorry Windows users !
WINDOWS_AUDIO = ["wav"]

# Filter for audio files ================================================#

def cleanedbasename(f):
    title = os.path.splitext(os.path.basename(f))[0]

    for noise in ["-", "_"]:
        title = title.replace(noise, " ")

    return title

def onlyimages(f):
    exts = [
        "bmp", "gif", "jpg", "jpeg", "png", "svg", "tiff", "webp", "xpm"
    ]

    for ext in exts:
        if f.lower().endswith(ext):
            return True

    return False

def onlyaudio(f):
    global GNULINUX_AUDIO
    global WINDOWS_AUDIO

    if os.name == "nt":
        exts = WINDOWS_AUDIO
    else:
        exts = GNULINUX_AUDIO

    for ext in exts:
        if f.lower().endswith(ext):
            return True

    return False

# XML functions =========================================================#

def fromxml_yesno(v):
    if v.lower().strip() == "yes":
        return True
    else:
        return False

def toxml_yesno(v):
    if v:
        return "yes"
    else:
        return "no"

# vim:set shiftwidth=4 softtabstop=4:
