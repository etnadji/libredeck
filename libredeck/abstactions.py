#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
Abstract actions classes for LibreDeck.
"""

# Imports ===============================================================#

import os
import subprocess

#--- GTK ----------------------------------------------

import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk

#--- LXML ---------------------------------------------

import lxml.etree as ET

#--- LibreDeck ----------------------------------------

import libredeck.handlers as ldhandlers
import libredeck.mixins as ldmixins

from libredeck.common import fromxml_yesno,toxml_yesno

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Classes ===============================================================#

class AbstractActionHandler(ldhandlers.BaseHandler):
    """
    Abstract class for an action

    :type builder: Gtk.Builder
    :param builder: GTK Builder
    :type application: ldeck.LibreDeck
    :param application: LibreDeck application
    :type deck: ldeck.DeckHandler
    :param deck: Current deck/file
    """

    def __init__(self, builder, application, deck):
        super().__init__(builder, application)

        self.deck = deck

        self.action_type = False
        self.action_configured = False

        self.config = {
            "object": False,
            "file": False,
            "builder": False
        }

        self._cdialog = {
            "dialog": "actionConfigDialog",
            "notebook": "notebook",
            # "file": "ui/abstractaction.glade",
            "file": "abstractaction",
            "builder": False,
            "window": False,
        }

        self.mixins = {}

        self.datas = {}

    #=== XML =============================================================

    def toxml(self):
        xml = ET.Element("action")

        if self.action_type:
            xml.attrib["type"] = self.action_type

        return xml

    def fromxml(self, xml):
        if xml.tag == "action":

            if (action_type := xml.get("type")) is not None:
                self.action_type = action_type

            return True

        return False

    #=== Glade ===========================================================

    def _gladeobj(self, objname):
        return self.builder.get_object(objname)

    def _gladeconfobj(self, objname):
        return self.config["builder"].get_object(objname)

    #=== Method defining default attributes (for new action) =============

    def default(self):
        pass

    #=== To add a tab to the action config dialog ========================

    def add_to_config_notebook(self, widget, label):
        notebook = self._cdialog["builder"].get_object(
            self._cdialog["notebook"]
        )

        add_page = False

        page_labels = []
        pages = notebook.get_n_pages()

        if pages:
            for p in range(pages+1):
                try:
                    l = notebook.get_tab_label_text(
                        notebook.get_nth_page(p)
                    )
                    page_labels.append(l)
                except TypeError:
                    pass

        if label not in page_labels:
            add_page = True

        if add_page:
            notebook.append_page(widget, Gtk.Label(label))

    #=== Config dialog validate/cancel callbacks and methods =============

    def on_ac_cancel_clicked(self, button):
        self.cancel_config()

    def on_ac_validate_clicked(self, button):
        self.validate_config()

    def validate_config(self):
        return True

    def cancel_config(self):
        return True

    #=== Prepare the config dialog =======================================

    def prepare_config(self):

        do_prepare = True

        if self._cdialog["file"]:
            if not self._cdialog["builder"]:
                self._cdialog["builder"] = Gtk.Builder()
                self._cdialog["builder"].add_from_file(
                    self.application.ui_file(self._cdialog["file"])
                )
                self._cdialog["builder"].connect_signals(self)
        else:
            do_prepare = False

        if do_prepare:

            if self.config["file"]:

                if not self.config["builder"]:
                    self.config["builder"] = Gtk.Builder()
                    self.config["builder"].add_from_file(
                        self.application.ui_file(self.config["file"])
                    )
                    self.config["builder"].connect_signals(self)

            notebook = self._cdialog["builder"].get_object(
                self._cdialog["notebook"]
            )

            return True

        return False

    #=== Show / Hide config dialog =======================================

    def show_config(self):
        self.prepare_config()

        self._cdialog["window"] = self._cdialog["builder"].get_object(
            self._cdialog["dialog"]
        )
        self._cdialog["window"].set_transient_for(self.application.win)

        self._cdialog["window"].run()

    def close_config(self):
        self._cdialog["window"] = self._cdialog["builder"].get_object(
            self._cdialog["dialog"]
        )
        self._cdialog["window"].hide()


class AbstractSwitchAction(
        AbstractActionHandler,
        ldmixins.MixinActionTitle,
        ldmixins.MixinKeyboardShortcut):
    """
    Abstract class for an action with a title and a switch

    :type application: ldeck.LibreDeck
    :param application: LibreDeck application
    :type deck: ldeck.DeckHandler
    :param deck: Current deck/file
    """

    def __init__(self, application, deck):
        # Abstract action handler
        AbstractActionHandler.__init__(self, False, application, deck)
        self.builder_file = "switchaction"
        # self.builder_file = "ui/switchaction.glade"
        self.config["object"] = "config_tab"

        # Action with a title
        ldmixins.MixinActionTitle.__init__(self)
        self.title_object = "actionLabel"

        # Action with a keyboard shortcut
        ldmixins.MixinKeyboardShortcut.__init__(self)

        # Specific config
        self.switch_save = False

    def fromxml(self, xml):
        AbstractActionHandler.fromxml(self, xml)
        ldmixins.MixinActionTitle.fromxml(self, xml)
        ldmixins.MixinKeyboardShortcut.fromxml(self, xml)

        if self.switch_save:
            for element in xml:
                if element.tag == "state":
                    self.switch_save = fromxml_yesno(element.text)

    def toxml(self):
        xml = AbstractActionHandler.toxml(self)

        title = ldmixins.MixinActionTitle.toxml(self)
        xml.append(title)

        shortcut = ldmixins.MixinKeyboardShortcut.toxml(self)
        xml.append(shortcut)

        if self.switch_save:
            state = ET.Element("state")
            state.text = toxml_yesno(self.get_state())
            xml.append(state)

        return xml

    def init_gui(self):
        AbstractActionHandler.init_gui(self)
        ldmixins.MixinActionTitle.init_gui(self)

        if self.switch_save:
            self.set_state(self.saved_state)

        self.widget = self._gladeobj("actionWidget")
        self.widget.show_all()

        self.deck.box.add(self.widget)

    def prepare_config(self):
        AbstractActionHandler.prepare_config(self)
        ldmixins.MixinActionTitle.prepare_config(self)
        ldmixins.MixinKeyboardShortcut.prepare_config(self)
        # Here prepare specific UI in switchaction.glade

    def set_title(self, title, bold=False, italic=False, color=False):
        ldmixins.MixinActionTitle.set_title(self, title, bold, italic, color)

    def validate_config(self):
        ldmixins.MixinActionTitle.validate_config(self)
        ldmixins.MixinKeyboardShortcut.validate_config(self)
        AbstractActionHandler.close_config(self)

    def cancel_config(self):
        AbstractActionHandler.close_config(self)

    def get_state(self):
        state = self._gladeobj("actionSwitch").get_state()
        self.saved_state = state
        return state

    def set_state(self, state):
        return self._gladeobj("actionSwitch").set_state(state)

    def shortcuted(self, accelgroup, appwindow, key, flags):
        new_state = not self.get_state()
        self.set_state(new_state)

    def switch_on(self):
        print("ON")

    def switch_off(self):
        print("OFF")

    def switch_toggle(self, state):
        if state:
            self.switch_on()
        else:
            self.switch_off()

    def on_actionSwitch_state_set(self, switch, state):
        self.switch_toggle(state)


class AbstractButtonAction(
        AbstractActionHandler,
        ldmixins.MixinActionTitle,
        ldmixins.MixinActionButton,
        ldmixins.MixinKeyboardShortcut):
    """
    Abstract class for an action with a title and a button

    :type application: ldeck.LibreDeck
    :param application: LibreDeck application
    :type deck: ldeck.DeckHandler
    :param deck: Current deck/file
    """

    def __init__(self, application, deck):
        # Abstract action handler
        AbstractActionHandler.__init__(self, False, application, deck)
        self.builder_file = "baseaction"

        # Action with a title
        ldmixins.MixinActionTitle.__init__(self)
        self.title_object = "actionLabel"

        # Action with a button
        ldmixins.MixinActionButton.__init__(self)
        self.button_object = "actionButton"

        # Action with a keyboard shortcut
        ldmixins.MixinKeyboardShortcut.__init__(self)

    def fromxml(self, xml):
        AbstractActionHandler.fromxml(self, xml)
        ldmixins.MixinActionTitle.fromxml(self, xml)
        ldmixins.MixinActionButton.fromxml(self, xml)
        ldmixins.MixinKeyboardShortcut.fromxml(self, xml)

    def toxml(self):
        xml = AbstractActionHandler.toxml(self)

        title = ldmixins.MixinActionTitle.toxml(self)
        xml.append(title)

        button = ldmixins.MixinActionButton.toxml(self)
        xml.append(button)

        shortcut = ldmixins.MixinKeyboardShortcut.toxml(self)
        xml.append(shortcut)

        return xml

    def init_gui(self):
        AbstractActionHandler.init_gui(self)
        ldmixins.MixinActionTitle.init_gui(self, self.application.settings.ui["hide_button_title"])
        # ldmixins.MixinActionTitle.init_gui(self)
        ldmixins.MixinActionButton.init_gui(self)

        self.widget = self._gladeobj("actionWidget")
        self.widget.show_all()

        self.deck.box.add(self.widget)

    def click(self):
        print("Click")

    def shortcuted(self, accelgroup, appwindow, key, flags):
        self.click()

    def prepare_config(self):
        AbstractActionHandler.prepare_config(self)
        ldmixins.MixinActionTitle.prepare_config(self)
        ldmixins.MixinActionButton.prepare_config(self)
        ldmixins.MixinKeyboardShortcut.prepare_config(self)

    def validate_config(self):
        ldmixins.MixinActionTitle.validate_config(self)
        ldmixins.MixinActionButton.validate_config(self)
        ldmixins.MixinKeyboardShortcut.validate_config(self)
        AbstractActionHandler.close_config(self)

    def cancel_config(self):
        AbstractActionHandler.close_config(self)

    def set_mode(self, mode, ui=True):
        ldmixins.MixinActionButton.set_mode(self, mode, ui)

    def set_button_image(self, filename):
        ldmixins.MixinActionButton.set_button_image(self, filename)

    def set_button_label(self, label="", keep=True):
        ldmixins.MixinActionButton.set_button_label(self, label, keep)

    def on_actionButton_clicked(self, button):
        self.click()

    def set_title(self, title, bold=False, italic=False, color=False):
        ldmixins.MixinActionTitle.set_title(self, title, bold, italic, color)

# vim:set shiftwidth=4 softtabstop=4:
