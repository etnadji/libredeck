#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
Install LibreDeck on Windows
"""

# Imports ===============================================================#

import os
import sys
import shutil

INSTALL = True

#--- Third parties : appdirs -----------------------------------

try:
    from appdirs import user_config_dir
except ModuleNotFoundError:
    print("- LibreDeck requires appdirs. Please install it.")
    INSTALL = False

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Fonctions =============================================================#

def makeCmd(username, ):
    desktop_cmd = "/".join(
        ["C:", "Documents and Settings", username, "Desktop", "LibreDeck.cmd"]
    )

    with open(desktop_cmd, "w", encoding="utf8") as dc:
        dc.write("@echo off\r\n")
        dc.write("C:\\msys64\\usr\\bin\\mintty.exe ")
        dc.write("-w hide /bin/env MSYSTEM=MINGW64 ")
        dc.write("/mingw64/bin/python3 ")
        dc.write("/home/{}/libredeck/launcher.py".format(username))

        print("- Desktop shortcut added.")

def makeDirs(config_dir):
    try:
        os.makedirs(config_dir)
        print("- LibreDeck user directory created")
    except FileExistsError:
        pass

    return True

def install():
    print(60*"=")
    print("LibreDeck net-install...\n")

    USERNAME = os.getlogin()
    CONFIG_DIR = os.sep.join([user_config_dir(), "LibreDeck"])

    print("Username :", USERNAME)
    print("LibreDeck user directory :", CONFIG_DIR)

    print("")

    dirs_ok = makeDirs(CONFIG_DIR)

    if dirs_ok:
        makeCmd(USERNAME)

    print("LibreDeck is installed.")

    print(60*"=")

# Programme =============================================================#

if __name__ == "__main__":

    if INSTALL:
        install()

    sys.exit(0)

# vim:set shiftwidth=4 softtabstop=4:
