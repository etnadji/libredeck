#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
Update LibreDeck in Windows
"""

# Global variables ======================================================#

UPDATE = True

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Imports ===============================================================#

import os
import sys
import shutil
import subprocess
import io

#--- Third parties : appdirs -----------------------------------

try:
    from appdirs import user_config_dir
except ModuleNotFoundError:
    print("- LibreDeck requires appdirs. Please install it.")
    UPDATE = False

# Classes ===============================================================#

# Fonctions =============================================================#

def git(data_dir):
    subprocess.Popen(
        ["git", "pull", "origin", "master"],
        cwd=data_dir,
    )

def update():
    print(60*"=")
    print("LibreDeck updating...\n")

    DATA_DIR = os.sep.join(
        os.path.realpath(__file__).split(os.sep)[:-2]
    )

    USERNAME = os.getlogin()
    CONFIG_DIR = os.sep.join([user_config_dir(), "LibreDeck"])

    git(DATA_DIR)

# Programme =============================================================#

if __name__ == "__main__":

    if UPDATE:
        update()

    sys.exit(0)

# vim:set shiftwidth=4 softtabstop=4:
