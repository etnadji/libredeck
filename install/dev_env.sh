#!/usr/bin/env bash

set -e

# Taken from Gajim build process
# https://dev.gajim.org/gajim/gajim/-/blob/master/win/dev_env.sh

function main {
    pacman --noconfirm -S --needed \
        git \
        mingw-w64-x86_64-python \
        mingw-w64-x86_64-python-gobject \
        mingw-w64-x86_64-python-pip \
        mingw-w64-x86_64-toolchain \
        mingw-w64-x86_64-adwaita-icon-theme \
        mingw-w64-x86_64-gtk3 \
        mingw-w64-x86_64-python-lxml \
        mingw-w64-x86_64-python-appdirs
}

main;
