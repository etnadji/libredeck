#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
Launch LibreDeck
"""

from libredeck import libredeck

libredeck.main()

# vim:set shiftwidth=4 softtabstop=4:
