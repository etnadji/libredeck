#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
LibreDeck, a virtual streamdeck for GNOME.
"""

RUN_LIBREDECK = True

# Imports ===============================================================#

import os
import sys

#--- appdirs ------------------------------------------

try:
    from appdirs import user_config_dir
except ModuleNotFoundError:
    print("- LibreDeck requires appdirs. Please install it.")

#--- LibreDeck ----------------------------------------

import libredeck.config as ldconfig
import libredeck.app as ldapp

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Programme =============================================================#

def main(arguments):
    #--- Configuration ------------------------------------

    EXEC_PATH = os.path.realpath(__file__)
    UI_PATH = os.sep.join(EXEC_PATH.split(os.sep)[:-1] + ["ui"])

    if os.name == "nt":
        # Skipping the application author folder used on Windows
        # => /USER/AppData/Local/LibreDeck
        CONFIG_DIR = os.sep.join([user_config_dir(), "LibreDeck"])
    else:
        # Usually ~/.config/LibreDeck/
        CONFIG_DIR = user_config_dir("LibreDeck")

    if not os.path.exists(CONFIG_DIR):
        os.makedirs(CONFIG_DIR)

    CONFIG_FILE = "{}{}settings.xml".format(CONFIG_DIR, os.sep)

    config = ldconfig.Settings(CONFIG_FILE)

    if os.path.exists(CONFIG_FILE):
        # Just read the config file or...
        config.fromfile()
    else:
        # ... Creates it at first startup
        config.tofile()

    #--- Load GUI and setup handler -----------------------

    ldeck = ldapp.LibreDeck(config, UI_PATH, arguments)
    ldeck.init_gui()

    #--- Running GUI --------------------------------------

    ldeck.show_all()

    #--- End ----------------------------------------------

    return 0

# Programme =============================================================#

if __name__ == "__main__":

    if RUN_LIBREDECK:
        if ldapp.RUN_LIBREDECK:
            code = main(sys.argv)
            sys.exit(code)

    sys.exit(1)

# vim:set shiftwidth=4 softtabstop=4:
