clean:
	-rm ui/*.glade~
	-rm libredeck/__pycache__/*
	-rmdir libredeck/__pycache__
	-rm __pycache__/*
	-rmdir __pycache__
